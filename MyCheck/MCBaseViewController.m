//
//  MCBaseViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCPaymentMethodViewController.h"
#import "UIViewController+Transitions.h"
#import "MCCreditCardViewController.h"
#import "MyCheck.h"
#import "NSBundle+MCBundle.h"
#import "MCUserDefaults.h"

#define NotNowButtonIndex 0
#define AddACardButtonIndex 1

@interface MCBaseViewController () <UIAlertViewDelegate>

@end

@implementation MCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topBar.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setAppearence];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [super viewWillAppear:animated];
}

#pragma MCTopBarDelegate

-(IBAction)back:(id)sender{
    UIViewController *poppedViewController = [self.navigationController popViewControllerAnimated:YES];
    if (!poppedViewController) {
        [self.navigationController dismissViewControllerWithPushAnimationAndCompletion:nil];
        [self resetAppearance];
    }
}

-(void)loggedOut{
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[MyCheck sharedInstance].userDefaults removeValueForKey:@"userEmail"];
    [MyCheck sharedInstance].loggedIn = NO;
}

-(void)failedToLogout{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Logout failure" message:@"Failed to log out" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

-(void)popToViewControllerWithClass:(Class)class{
    if ([self isKindOfClass:class]) {
        return;
    }
    NSArray *viewControllers = [[self navigationController] viewControllers];
    for( int i=0;i<[viewControllers count];i++){
        id obj=[viewControllers objectAtIndex:i];
        if([obj isKindOfClass:class]){
            [[self navigationController] popToViewController:obj animated:YES];
            return;
        }
    }
}

-(void)showNoPaymentAlert{
    UIAlertView *backAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your card details to check-in and use Pay at Table" delegate:self cancelButtonTitle:@"Not now" otherButtonTitles:@"Add a card", nil];
    [backAlert show];
}

-(void)notNowPressed{
    [self popToViewControllerWithClass:[MCPaymentMethodViewController class]];
}

#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (buttonIndex) {
        case NotNowButtonIndex:
            [self notNowPressed];
            break;
        case AddACardButtonIndex: {
            MCCreditCardViewController *creditCardViewController = [[MCCreditCardViewController alloc] initWithNibName:@"MCCreditCardViewController" bundle:[NSBundle mycheckBundle]];
            [self.navigationController pushViewController:creditCardViewController animated:YES];
        }
        break;
        default:
            break;
    }
}


-(void)resetAppearance{
    NSData *colorData = [[MyCheck sharedInstance].userDefaults valueForKey:@"originalTextFieldTint"];
    if(colorData){
        UIColor *color = [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
         [[UITextField appearance] setTintColor:color];
         [[UIButton appearance] setTintColor:color];
         [[UIAlertView appearance] setTintColor:color];
    }
}

-(void)setAppearence{
    NSData *colorData = [[MyCheck sharedInstance].userDefaults valueForKey:@"originalTextFieldTint"];
    if (!colorData) {
        UIColor* color =[UITextField appearance].tintColor;
        [NSKeyedArchiver archivedDataWithRootObject:color];
    }
    [[MyCheck sharedInstance].userDefaults setValue:colorData forKey:@"originalTextFieldTint"];
    [[UITextField appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
    [[UIButton appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
    [[UIAlertView appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
