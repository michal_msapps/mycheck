//
//  FileStoreManager.m
//  Drippler
//
//  Created by Drippler on 8/24/14.
//  Copyright (c) 2014 Drippler. All rights reserved.
//

#import "MSFileStoreManager.h"

@implementation MSFileStoreManager

+(NSError *)createFileIfDoesNotExistWithName:(NSString *)name andExtension:(NSString *)type{
    NSError *error;
    NSString *path = [self pathForFileName:name withType:type];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSMutableDictionary *data;
    if (![fileManager fileExistsAtPath:path]) {
        data = [[NSMutableDictionary alloc] init];
    } else {
        data = [[NSMutableDictionary alloc] initWithContentsOfFile:path];
    }
    
    [data writeToFile:path atomically:NO];
    
    return error;
}


+(NSString *)pathForFileName:(NSString *)name withType:(NSString *)type{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = paths[0];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@", name, type];
    NSString *path = [documentDirectory stringByAppendingPathComponent:fileName];
    return path;
}

@end
