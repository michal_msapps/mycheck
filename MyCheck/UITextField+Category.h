//
//  UITextField+Category.h
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Category)

-(void)addNumberToolBar;

-(void)addNumberToolBarWithTarget:(id)target andAction:(SEL)action;

-(void)addNumberToolBarWithTarget:(id)target titles:(NSArray *)titles andActions:(NSArray *)actions;

@end
