//
//  MCBaseViewController.h
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCTopBar.h"
#import "MCBaseViewControllerWithKeyBoard.h"

@interface MCBaseViewController : MCBaseViewControllerWithKeyBoard <MCTopBarDelegate>

@property (nonatomic, strong) IBOutlet MCTopBar *topBar;

-(void)popToViewControllerWithClass:(Class)class;

-(void)showNoPaymentAlert;
-(void)resetAppearance;
@end
