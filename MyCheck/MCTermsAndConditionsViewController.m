//
//  MCTermsAndConditionsViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 5/15/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCTermsAndConditionsViewController.h"
#import "MyCheck.h"

@interface MCTermsAndConditionsViewController () <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation MCTermsAndConditionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.activityIndicatorView startAnimating];
    self.topBar.lblTitle.text = @"Terms and Conditions";
    NSString *termsAndConditionsURLString = [[MyCheck sharedInstance].userDefaults valueForKey:@"Terms and Conditions url"];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:termsAndConditionsURLString]]];
    self.webView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [self.activityIndicatorView removeFromSuperview];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failed loading web" message:@"Failed to load my check" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityIndicatorView removeFromSuperview];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
