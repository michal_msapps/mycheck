//
//  UIViewController+Transitions.h
//  MyCheck
//
//  Created by Michal Shatz on 3/29/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Transitions)

-(void)presentViewController:(UIViewController *)viewControler withPushAnimationAndCompletion:(void (^)(void))completion;

-(void)dismissViewControllerWithPushAnimationAndCompletion:(void (^)(void))completion;

@end
