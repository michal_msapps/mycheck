//
//  MCPaymentMethodViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCPaymentMethodViewController.h"
#import "MCMyCheckLoginViewController.h"
#import "MCTopBar.h"
#import "MyCheck.h"
#import "MCWebViewController.h"
#import "NSBundle+MCBundle.h"
#import <PayPal-iOS-SDK/PayPalConfiguration.h>
#import "UILabel+Category.h"
#import "MCAnalyticsStrings.h"

#define MyCheckBtnImageInsetsRatio 27/667
#define IPHONE6HEIGHT 667
#define IPHONE6WIDTH 375

@interface MCPaymentMethodViewController ()

@property (nonatomic, strong) IBOutlet UILabel *lblBookPayment1;
@property (nonatomic, strong) IBOutlet UILabel *lblBookPayment2;
@property (nonatomic, strong) IBOutlet UILabel *lblBookPayment3;
@property (nonatomic, strong) IBOutletCollection(UILabel) NSArray *lblsBookPayments;
@property (weak, nonatomic) IBOutlet UIButton *payPalButton;
@property (nonatomic, strong, readwrite) PayPalConfiguration *payPalConfiguration;
@property (nonatomic, strong) IBOutlet UIButton *myCheckButton;
@end

@implementation MCPaymentMethodViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if([MyCheck sharedInstance].appEventBlock) {
        [MyCheck sharedInstance].appEventBlock(NSStringFromClass([MCPaymentMethodViewController class]), MCChoosePaymentMethod, MCChoosePaymentMethodScreenWithPaymentMethodChoiceLoads);
    }
    self.topBar.lblTitle.text = @"Payment Method";
    self.topBar.delegate = self;
    NSString *bookPayment1String = [[MyCheck sharedInstance].userDefaults valueForKey:@"BookPayment1"];
    if (bookPayment1String && bookPayment1String.length > 0) {
        self.lblBookPayment1.text = bookPayment1String;
    }
    NSString *bookPayment2String = [[MyCheck sharedInstance].userDefaults valueForKey:@"BookPayment2"];
    if (bookPayment2String && bookPayment2String.length > 0) {
        self.lblBookPayment2.text = bookPayment2String;
    }
    
    float heightAspectRatio = [UIScreen mainScreen].bounds.size.height / IPHONE6HEIGHT;
    float widthAspectRatio = [UIScreen mainScreen].bounds.size.width / IPHONE6WIDTH ;
    self.myCheckButton.imageEdgeInsets = UIEdgeInsetsMake(heightAspectRatio * self.myCheckButton.imageEdgeInsets.top, widthAspectRatio * self.myCheckButton.imageEdgeInsets.left, heightAspectRatio * self.myCheckButton.imageEdgeInsets.bottom, widthAspectRatio * self.myCheckButton.imageEdgeInsets.right);
    UIEdgeInsets insets = UIEdgeInsetsMake(14, 14, 14, 14);

    UIImage* strchable =[[self.myCheckButton backgroundImageForState:UIControlStateNormal] resizableImageWithCapInsets:insets];
    [self.myCheckButton setBackgroundImage:strchable forState:UIControlStateNormal];
    [self.payPalButton setBackgroundImage:strchable forState:UIControlStateNormal];
   // [self.myCheckButton setTitle:@"Pay with" forState:UIControlStateNormal];
    //setting up paypal
    _payPalConfiguration = [[PayPalConfiguration alloc] init];
    _payPalConfiguration.merchantName = @"MyCheck";
    _payPalConfiguration.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://mycheckapp.com/us/site/privacy"];
    _payPalConfiguration.merchantUserAgreementURL = [NSURL URLWithString:@"https://mycheckapp.com/us/site/terms"];
    for (UILabel *label in self.lblsBookPayments) {
        [label setFont:[UIFont fontWithName:@"HelveticaNeue-Bold" size:label.font.pointSize] words:@[@"MyCheck", @"PayPal"]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)payWithPaypal:(id)sender {
    if([MyCheck sharedInstance].appEventBlock)
     [MyCheck sharedInstance].appEventBlock(NSStringFromClass([MCPaymentMethodViewController class]), MCChoosePaymentMethod, MCChoosePaymentMethodPaypalClicked);
    [AFComunication sharedAFComunication].isPayPal = YES;
    [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:[AFComunication sharedAFComunication].isPayPal] forKey:@"isPayPal"];
    NSSet *scopeValues = [NSSet setWithArray:@[@"https://uri.paypal.com/services/mis/customer",@"profile" ]];
    
    PayPalProfileSharingViewController *psViewController;
    psViewController = [[PayPalProfileSharingViewController alloc] initWithScopeValues:scopeValues
                                                                         configuration:self.payPalConfiguration
                                                                              delegate:self];
    // Present the PayPalProfileSharingViewController
    [self presentViewController:psViewController animated:YES completion:nil];
}

-(IBAction)payWithMyCheck:(id)sender{
    if([MyCheck sharedInstance].appEventBlock)
     [MyCheck sharedInstance].appEventBlock(NSStringFromClass([MCPaymentMethodViewController class]), MCChoosePaymentMethod, MCChoosePaymentMethodMycheckClicked);
    [AFComunication sharedAFComunication].isPayPal = NO;
     [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:[AFComunication sharedAFComunication].isPayPal] forKey:@"isPayPal"];
    if ([[MyCheck sharedInstance].userDefaults valueForKey:@"userEmail"] || [MyCheck sharedInstance].loggedIn) {
        [AFComunication sharedAFComunication].justEnteredPin = NO;
        if ([AFComunication sharedAFComunication].hasPaymentMethod) {
            MCWebViewController *mcWebViewController = [[MCWebViewController alloc] initWithNibName:@"MCWebViewController" bundle:[NSBundle mycheckBundle]];
            [self.navigationController pushViewController:mcWebViewController animated:YES];
        } else {
            [self showNoPaymentAlert];
        }
    } else {
        MCMyCheckLoginViewController *myCheckLoginViewController = [[MCMyCheckLoginViewController alloc] initWithNibName:@"MyCheckLoginView" bundle:[NSBundle mycheckBundle]];
        myCheckLoginViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        myCheckLoginViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [self.navigationController pushViewController:myCheckLoginViewController animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - PayPalProfileSharingDelegate methods

- (void)userDidCancelPayPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController {
    // User cancelled login. Dismiss the PayPalProfileSharingViewController, breathe deeply.
    [self resetAppearance];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)payPalProfileSharingViewController:(PayPalProfileSharingViewController *)profileSharingViewController
             userDidLogInWithAuthorization:(NSDictionary *)profileSharingAuthorization {
    // The user has successfully logged into PayPal, and has consented to profile sharing.
    
    [self sendAuthorizationToServer:profileSharingAuthorization];
}

- (void)sendAuthorizationToServer:(NSDictionary *)consentJSONData {
    // Send the entire authorization reponse
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:consentJSONData
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
      NSString* metadataId=  [PayPalMobile clientMetadataID];
        AFComunication* com = [AFComunication sharedAFComunication];
        [com authenticatePayPalWithAuthCode:jsonString  locationId:nil metaDataId:metadataId success:^(){
            [AFComunication sharedAFComunication].justEnteredPin = YES;
            [AFComunication sharedAFComunication].isPayPal = YES;
             [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:[AFComunication sharedAFComunication].isPayPal] forKey:@"isPayPal"];
            MCWebViewController *mcWebViewController = [[MCWebViewController alloc] initWithNibName:@"MCWebViewController" bundle:[NSBundle mycheckBundle]];
            [[MyCheck sharedInstance].userDefaults setValue:@"PayPalUser" forKey:@"userEmail"];
            [self.navigationController pushViewController:mcWebViewController animated:YES];
            [self dismissViewControllerAnimated:YES completion:nil];
            [self resetAppearance];

        
        } failure:^(MCResponseData *response, NSError *error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"PayPal Authorization failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }];
    }
}
@end
