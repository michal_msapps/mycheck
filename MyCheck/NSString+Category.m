//
//  NSString+Category.m
//  MyCheck
//
//  Created by Michal Shatz on 4/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "NSString+Category.h"

@implementation NSString (Category)

+(NSString *)stringWithBool:(BOOL)booleanValue{
    return (booleanValue) ? @"True" : @"False";
}

-(BOOL)validateEmail {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

@end
