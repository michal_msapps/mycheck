//
//  MCBaseViewControllerWithKeyBoard.h
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MSBaseViewControllerWithKeyboard.h"

@interface MCBaseViewControllerWithKeyBoard : MSBaseViewControllerWithKeyboard <UITextFieldDelegate>

@end
