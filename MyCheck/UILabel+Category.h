//
//  UILabel+Category.h
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Category)

-(void)setFont:(UIFont *)fontForWords words:(NSArray *)words;

@end
