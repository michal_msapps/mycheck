//
//  MCTopBar.m
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCTopBar.h"
#import "MSButtonWithSubButtons.h"
#import "AFComunication.h"
#import "MyCheck.h"
#import "NSBundle+MCBundle.h"

@interface MCTopBar ()

@property (nonatomic, strong) IBOutlet UIView *containerView;

@end

@implementation MCTopBar

-(IBAction)back:(id)sender{
    [self.delegate back:sender];
}

-(void)awakeFromNib{
    [[NSBundle mycheckBundle] loadNibNamed:@"MCTopBar" owner:self options:nil];
    self.containerView.frame = self.bounds;
    [self addSubview:self.containerView];
    [super awakeFromNib];
}

-(IBAction)logout:(id)sender{
    if ([MyCheck sharedInstance].loggedIn) {
        [[AFComunication sharedAFComunication] logoutSuccess:^{
            [self.delegate loggedOut];
        } failure:^(MCResponseData *response, NSError *error) {
            [self.delegate failedToLogout];
        }];
    } else {
        [self.delegate loggedOut];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
