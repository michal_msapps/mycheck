//
//  Order.h
//  MyCheck
//
//  Created by Elad S on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCOrder : NSObject {
    NSString* BizName;
    NSString* OrderID;
    NSString* OrderTime;
    NSString* Date;
    float TaxPercent;
    float TaxAmount;
    NSString* SubTotal;
    NSArray* Items;
    NSDictionary* dictionary;
    NSString*Paid;
    NSString* Price;
    NSString* Service;
    NSString* Balance;
    NSString* tableCode;
    NSString* User;
    NSString* BID;
    BOOL noTip;
    float tipPercent;
    BOOL quickPay;
    NSString* status;
    BOOL showFeedback;
    float paidTip;
    float MaxTip;

}
@property(readwrite,nonatomic)float calculatedSubtotal;
@property(readwrite,nonatomic)float calculatedTotal;
@property(readwrite,nonatomic)float calculatedTax;
@property(readwrite,nonatomic)float calculatedTip;


@property(retain,nonatomic)    NSString*Html;

@property(nonatomic,readwrite) float MaxTip;

@property(nonatomic,strong)NSMutableArray* selectedItemsJson;

@property(readwrite,nonatomic)float servicePercent;
@property(nonatomic,readwrite) float paidTip;

@property(readwrite)BOOL showFeedback;
@property(strong,nonatomic)NSString* status;

@property(strong,nonatomic)NSString* BID;
@property(strong,nonatomic)NSString* locationId;

@property(strong,nonatomic)NSString* User;

@property(strong,nonatomic)NSString* tableCode;
@property(strong,nonatomic) NSString* BizName;
@property(strong,nonatomic) NSString* OrderID;
@property(strong,nonatomic) NSString* OrderTime;
@property(strong,nonatomic) NSString* Date;
@property(readwrite,nonatomic) float TaxPercent;
@property(readwrite,nonatomic) float TaxAmount;
@property(readwrite,nonatomic) float tipPercent;

@property(strong,nonatomic) NSString* SubTotal;
@property(strong,nonatomic) NSArray* Items;
@property(strong,nonatomic) NSDictionary* dictionary;
@property(strong,nonatomic)NSString*Paid;
@property(strong,nonatomic)NSString*iPaid;

@property(strong,nonatomic)NSString* Price;
@property(strong,nonatomic)NSString* Service;
@property(strong,nonatomic)NSString* Balance;
@property(readwrite)BOOL noTip;
@property(readwrite)BOOL quickPay;

-(id)initWithDic:(NSDictionary*)dic;
@end
