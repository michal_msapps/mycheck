//
//  BaseViewControllerWithKeyboard.m
//  BBShower
//
//  Created by Michal Shatz on 12/11/13.
//  Copyright (c) 2013 Michal Shatz. All rights reserved.
//

#import "MSBaseViewControllerWithKeyboard.h"
#import "MCPaymentMethodViewController.h"
#define IS_IPHONE_5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

@interface MSBaseViewControllerWithKeyboard () <UITextFieldDelegate>
{
    BOOL keyboardShown;
}
@property (nonatomic, strong) NSNotification *notification;
@property (nonatomic, assign) BOOL frameChanged;
@property (nonatomic, assign) CGSize initialScrollViewContentSize;

@end

@implementation MSBaseViewControllerWithKeyboard

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self registerForKeyboardNotifications];
    self.scrollView.frame = [UIScreen mainScreen].bounds;
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.bottomOfScroll.constant=0;
}

-(void)viewDidAppear:(BOOL)animated{
    self.initialScrollViewContentSize = self.scrollView.contentSize;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification
{
    self.notification = aNotification;
   NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
//    
//    // If active text field is hidden by keyboard, scroll it so it's visible
//    // Your app might not need or want this behavior.
    CGRect aRect = [UIScreen mainScreen].bounds;
    aRect.size.height -= kbSize.height;
    if (self.activeField.inputAccessoryView) {
        aRect.size.height -= self.activeField.inputAccessoryView.frame.size.height;
    }
    CGRect bkgndRect = self.activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
   // CGRect bkgnRectWithInsets = CGRectInset(bkgndRect, 0, -40);
    CGPoint pointInSuperview = [self.view.superview convertPoint:_activeField.frame.origin fromView:self.view];
    pointInSuperview.y += self.activeField.frame.size.height;
    if (!CGRectContainsPoint(aRect, pointInSuperview)) {
      //  [self.activeField.superview setFrame:bkgnRectWithInsets];
        self.frameChanged = YES;
        CGFloat kbSizeHight = kbSize.height;
        if (self.view.frame.size.height == 480) {
            kbSizeHight = self.view.frame.size.height / 3;
        }
        if (self.activeField.inputAccessoryView) {
            kbSizeHight -= self.activeField.inputAccessoryView.frame.size.height;
        }
        [self.scrollView setContentOffset:CGPointMake(0.0, self.activeField.frame.origin.y-kbSizeHight) animated:YES];
//        CGSize scrollViewContentSize = self.scrollView.contentSize;
//        scrollViewContentSize.height = kbSizeHight;
//        self.scrollView.contentSize = scrollViewContentSize;
    }
    
    
    
    
    //resizing scrollview
    
    if(self.bottomOfScroll.constant == 0){
        self.bottomOfScroll.constant = (kbSize.height+40 );
        self.scrollView.scrollEnabled=YES;
        [self.view layoutIfNeeded];
       // CGSize scrollContentSize = CGSizeMake(320, 580);
        
      //  self.scrollView.contentSize = scrollContentSize;
    }
    //}
    
    keyboardShown = YES;
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    self.frameChanged = NO;
    
    if(self.bottomOfScroll.constant != 0){
        self.bottomOfScroll.constant =0;
        self.scrollView.scrollEnabled=YES;
        [self.view layoutIfNeeded];
        if(IS_IPHONE_5){
            CGSize scrollContentSize = CGSizeMake(320, 100);
            
            self.scrollView.contentSize = scrollContentSize;
            
        }
        
       // self.scrollView.contentOffset = originalOffset;
        
    }//
    
    keyboardShown = NO;
    
    [self resetScroll];

}

-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.activeField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    self.activeField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    UITextField *nextTextField = (UITextField *)[self.scrollView viewWithTag:self.activeField.tag + 1];
    if (nextTextField) {
        [nextTextField becomeFirstResponder];
        self.activeField = nextTextField;
    }else{
        [self.activeField resignFirstResponder];

    }
    return YES;
}

-(IBAction)dismissKeyboard:(id)sender{
    [self.activeField resignFirstResponder];
}

-(void)viewWillDisappear:(BOOL)animated{
    self.activeField = nil;
    [super viewWillDisappear:animated];
}

-(void)resetScroll{
    self.scrollView.contentSize = self.initialScrollViewContentSize;
   // [self.scrollView setContentOffset:CGPointZero animated:YES];
  //  [self.scrollView scrollsToTop];
    CGPoint bottomOffset = CGPointMake(0, self.scrollView.contentSize.height - self.scrollView.bounds.size.height);
    [self.scrollView setContentOffset:bottomOffset animated:YES];
}

@end
