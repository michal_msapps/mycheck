//
//  MCWebViewController.h
//  MyCheck
//
//  Created by Michal Shatz on 3/15/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"

@interface MCWebViewController : MCBaseViewController
@property (nonatomic,strong)NSString* tmpPin;

@end
