//
//  MSButtonWithSubButtons.h
//  MSButtonWithSubButtons
//
//  Created by Michal Shatz on 3/29/15.
//  Copyright (c) 2015 MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MSButtonWithSubButtons.
FOUNDATION_EXPORT double MSButtonWithSubButtonsVersionNumber;

//! Project version string for MSButtonWithSubButtons.
FOUNDATION_EXPORT const unsigned char MSButtonWithSubButtonsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <MSButtonWithSubButtons/PublicHeader.h>

@interface MSButtonWithSubButtons : UIButton

-(IBAction)buttonPressed:(id)sender;

@property (nonatomic, strong) IBOutletCollection(UIButton) NSArray *subButtons;

@end

