//
//  MCNavigationController.h
//  MyCheck
//
//  Created by Michal Shatz on 3/19/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"

@interface MCNavigationController : MCBaseViewController

@end
