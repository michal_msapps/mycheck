//
//  MCAnalyticsStrings.h
//  MyCheck
//
//  Created by Michal Shatz on 4/26/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#ifndef MyCheck_MCAnalyticsStrings_h
#define MyCheck_MCAnalyticsStrings_h

// Actions
static NSString *MCChoosePaymentMethod = @"ChoosePaymentMethod";

// Labels
static NSString *MCChoosePaymentMethodPaypalClicked = @"PaypalClicked";
static NSString *MCChoosePaymentMethodMycheckClicked = @"MycheckClicked";
static NSString *MCChoosePaymentMethodScreenWithPaymentMethodChoiceLoads = @"ScreenWithPaymentMethodChoiceLoads";

#endif
