//
//  MCUserDefaults.h
//  MyCheck
//
//  Created by Michal Shatz on 3/24/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCUserDefaults : NSObject

-(id)valueForKey:(NSString *)key;
-(void)setValue:(id)value forKey:(NSString *)key;
-(void)removeValueForKey:(NSString *)key;

@end
