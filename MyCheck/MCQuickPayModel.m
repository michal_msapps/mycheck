//
//  QuickPayModel.m
//  MyCheck
//
//  Created by Elad S on 2/21/13.
//
//

#import "MCQuickPayModel.h"
#import "AFComunication.h"
#import "MCOrder.h"
@implementation MCQuickPayModel (private)
int fails;
BOOL polling;
float seconds;
BOOL showedPaymentErrorMsg;
BOOL showedOpenError;
@end
@implementation MCQuickPayModel
@synthesize currentStatus,delegate;

-(id)initWithDelegate:(id<PaymentStatusDelegate>)del pollInterval:(float)interval{
    self=[super init];
    if (self) {
        seconds=interval;//[defaults floatForKey:@"QuickPayInterval"];
        self.delegate=del;
        if(seconds<=0)
            seconds=4;
    }
    return self;    
}
-(void)startPolling{
    if(polling)
        return;
    polling=true;
    
    [self getOrderDetails];
 
        //[comunication ViewOrder:nil quick:true loadingView:NO];
        

}

-(void) getOrderDetails{

    
//    NSMutableArray* arr= [[NSMutableArray alloc]initWithCapacity:8];
//    [arr addObject:[NSString stringWithFormat:@"Quick=1"]];
//    if(self.code)
//        [arr addObject:[NSString stringWithFormat:@"Code=%@",self.code]];
//    [comunication sendRequest:VIEW_ORDER withArgs:arr load:NO];
    // [comunication ViewOrder:nil quick:true loadingView:NO];
    
    
    
    AFComunication* com = [AFComunication sharedAFComunication];
    static NSURLSessionDataTask* task = nil;
    [task cancel];
    
    task =  [com getOrderDetailsForOrderID:nil tableCode:self.code polling:YES success:
             ^(MCOrder* order){
                 NSString* status=order.status;
                 if(![status isEqualToString:self.currentStatus] ){
                     self.currentStatus=status;
                     PaymentStatus ps=PENDING;
                     if([currentStatus caseInsensitiveCompare:@"Closed"]==NSOrderedSame ||[currentStatus caseInsensitiveCompare:@"Cancelled"]==NSOrderedSame )
                         ps=DONE;
                     if([currentStatus caseInsensitiveCompare:@"OPEN"]==NSOrderedSame)
                         ps=OPEN;
                     self.code=nil;
                     //appDel.order=order;
                     [delegate PaymentStatusChanged:ps order:order];
                     if(ps==DONE){
                        // appDel.order=nil;
                         //appDel.orderOpen=NO;
                         polling=false;
                         delegate=nil;
                         return;
                     }
                 }else{
                     NSObject* del=(NSObject*)delegate;
                     if([del respondsToSelector:@selector(newOrder:)]){
                         [delegate newOrder:order];
                     }
                 }
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, seconds * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                     //   NSLog(@"fff");
                     if(polling){
                         [self getOrderDetails];
                     }
                 });
             } failure:^(MCResponseData *res, NSError *error){
                 NSInteger code = error.code;
                 if(code==27){
                     NSString* status=NSLocalizedString(@"No open Order",nil);
                     if([status caseInsensitiveCompare:currentStatus]!=NSOrderedSame){
                         self.currentStatus=status;
                         PaymentStatus ps=NO_OPEN_ORDER;
                         [delegate PaymentStatusChanged:ps order:nil];
                     }
                 }else if(code==228){
                     NSString* OpenError = error.localizedDescription;
                     if(!showedOpenError &&OpenError && OpenError.length>0){
//                         CostumAlertView *alert=[[CostumAlertView alloc]  initWithTitle:NSLocalizedString(@"ERROR",nil) message:OpenError delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
//                         [alert show];
                         showedOpenError=YES;
                     }
                     
                     NSString* status=NSLocalizedString(@"Pending",nil);
                     if([status caseInsensitiveCompare:currentStatus]!=NSOrderedSame){
                         self.currentStatus=status;
                         PaymentStatus ps=PENDING;
                         [delegate PaymentStatusChanged:ps order:nil];
                     }
                     
                 }else{
                     fails++;
                 }
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, seconds * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                     //   NSLog(@"fff");
                     if(polling){
                         [self getOrderDetails];
                     }
                 });
             
    
    
    
       }];
}
-(void)stopPolling{
    polling=false;
}
-(void)reset{
   // if(self.currentStatus){
self.currentStatus=nil;
    [self startPolling];
        polling=YES;

   // }

}
//-(void)recivedResponce:(BOOL)success :(NSString*)message : (NSDictionary*)data :(NSInteger)code{
//    if(!polling)
//        return;
//    if(success){
//        NSString* PaymentError=[data objectForKey:@"PaymentError"];
//        if(!showedPaymentErrorMsg &&PaymentError && PaymentError.length>0){
//            CostumAlertView *alert;
//            if(PaymentError.length<120)
//            alert=[[CostumAlertView alloc]  initWithTitle:NSLocalizedString(@"ERROR",nil) message:PaymentError delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
//            else
//                alert=[[CostumAlertView alloc]  initBigWithTitle:NSLocalizedString(@"ERROR",nil) message:PaymentError delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
//
//            [alert show];
//            showedPaymentErrorMsg=YES;
//        }
//        NSString* status=[data objectForKey:@"Status"];
//        if(![status isEqualToString:self.currentStatus] ){
//            self.currentStatus=status;
//            PaymentStatus ps=PENDING;
//            if([currentStatus caseInsensitiveCompare:@"Closed"]==NSOrderedSame ||[currentStatus caseInsensitiveCompare:@"Cancelled"]==NSOrderedSame )
//                ps=DONE;
//            if([currentStatus caseInsensitiveCompare:@"OPEN"]==NSOrderedSame)
//                ps=OPEN;
//            Order *order=[[Order alloc] initWithDic:data];
//            self.code=nil;
//            appDel.order=order;
//            [delegate PaymentStatusChanged:ps order:order];
//            if(ps==DONE){
//                appDel.order=nil;
//                appDel.orderOpen=NO;
//                polling=false;
//                delegate=nil;
//                return;
//            }
//        }else{
//            NSObject* del=(NSObject*)delegate;
//            if([del respondsToSelector:@selector(newOrder:)]){
//            Order *order=[[Order alloc] initWithDic:data];
//            [delegate newOrder:order];
//            }
//        }
//        
//    }else{
//        if(code==27){
//            NSString* status=NSLocalizedString(@"No open Order",nil);
//            if([status caseInsensitiveCompare:currentStatus]!=NSOrderedSame){
//                self.currentStatus=status;
//                PaymentStatus ps=NO_OPEN_ORDER;
//                [delegate PaymentStatusChanged:ps order:nil];
//            }
//        }else if(code==228){
//            NSString* OpenError=[data objectForKey:@"OpenError"];
//            if(!showedOpenError &&OpenError && OpenError.length>0){
//                CostumAlertView *alert=[[CostumAlertView alloc]  initWithTitle:NSLocalizedString(@"ERROR",nil) message:OpenError delegate:nil cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles:nil];
//                [alert show];
//                showedOpenError=YES;
//            }
//
//            NSString* status=NSLocalizedString(@"Pending",nil);
//            if([status caseInsensitiveCompare:currentStatus]!=NSOrderedSame){
//                self.currentStatus=status;
//                PaymentStatus ps=PENDING;
//                [delegate PaymentStatusChanged:ps order:nil];
//            }
//
//        }else{
//            fails++;
//        }
//    }
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, seconds * NSEC_PER_SEC), dispatch_get_current_queue(), ^{
//     //   NSLog(@"fff");
//        if(polling){
//            NSMutableArray* arr= [[NSMutableArray alloc]initWithCapacity:8];
//            [arr addObject:[NSString stringWithFormat:@"Quick=1"]];
//            if(self.code)
//                [arr addObject:[NSString stringWithFormat:@"Code=%@",self.code]];
//            [comunication sendRequest:VIEW_ORDER withArgs:arr load:NO];
//        }
//            //[comunication ViewOrder:nil quick:true loadingView:NO];
//
//    });
//}


@end
