//
//  MyCheck.h
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "MCUserDefaults.h"
#import "AFComunication.h"

/**
 * This document describes the API exposed by the MyCheck SDK (1.x) which the developers can use to integrate MyCheck support into their iOS applications. 
 */

//! Project version number for MyCheck.
FOUNDATION_EXPORT double MyCheckVersionNumber;

//! Project version string for MyCheck.
FOUNDATION_EXPORT const unsigned char MyCheckVersionString[];

typedef void (^logAppScreenViewBlock)(NSString *screen_name);

typedef void (^logAppEventBlock)(NSString *screen_name, NSString *event_name, NSString *event_value);

@interface MyCheck : NSObject

/*
 The location id used to create the MyCheck share instance
 */
@property (nonatomic, strong) NSString *locationId;

/*
 User defaults dictionary for storing and reading mycheck strings from mycheck server
 */
@property (nonatomic, strong) MCUserDefaults *userDefaults;

/*
 A boolean value that indicated if the user is logged in
 */
@property (nonatomic , assign) BOOL loggedIn;

/*
 A block for logging an analytic event
 */
@property (copy, atomic) logAppEventBlock appEventBlock;

/** Initialize MyCheck
 *
 * When initializing MyCheck you must pass these two tokens. You initialize MyCheck by adding the following lines in the implementation file for your app delegate, ideally at the top of application:didFinishLaunchingWithOptions. 
 *  @param appType This is your developer API Key
 *
 *  @param deviceId This is your device id
 */
+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL) prod;

+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL) prod logAppEventBlock:(logAppEventBlock)logAppEventBlock;

/** Initialize MyCheck
 *
 * When initializing MyCheck you must pass these two tokens. You initialize MyCheck by adding the following lines in the implementation file for your app delegate, ideally at the top of application:didFinishLaunchingWithOptions.
 *  @param appType This is your developer API Key
 *
 *  @param deviceId This is your device id
 *  
 *  @param logAppScreenViewBlock A block for handeling a view analytics event in MyCheck
 *
 *  @param logAppEventBlock A block for handeling an analytics event
 */
+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL)prod logAppScreenView:()logAppScreenView logAppEventBlock:(logAppEventBlock)logAppEventBlock;

/** Returns an instance of MyCheck
 *
 * When calling any MyCheck instance method you must use sharedInstance. 
 */
+(instancetype)sharedInstance;

/** Show the payment screen with all payment options
 *
 *
 * @param viewController viewController on which the MyCheck payment options screen will show up.
 */
-(void)showMyCheck:(UIViewController *)viewController;

/* checks wether or not a table is already open for the logged in user. if it does a locationId of the business the table was opened in is returned.
 @param open returns weather or not a table is open for the logged in user
 @param locationId the locationId the table is open at or nil if open == NO
 
 */
-(void)isTableOpenSuccess:(void (^)(BOOL open, NSString* locationId))success
                                   failure:(void (^)(MCResponseData * task, NSError *error))failure;

/* get a list of businesses arranged according to distance from the specified location
 @param location
 the location you want to find businesses around

 
 */

-(void)getNearbyBusinessWithLocation:(CLLocation*)location success:(void (^)( NSMutableArray* businesses))success
                                              failure:(void (^)(MCResponseData * response, NSError *error))failure;
/*
 call in order to tell if the user is logged in or not. the function may return an answer immediately or make an api call first
 
 @param success optional
 @param failure optinal
 */
-(void)isLoggedInSuccess:(void (^)(BOOL loggedIn))success
                 failure:(void (^)(MCResponseData * response, NSError *error))failure;
/* logout a user
 
 */
-(void)logoutSuccess:(void (^)())success
                              failure:(void (^)(MCResponseData * response, NSError *error))failure;
@end

