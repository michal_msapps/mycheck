//
//  Business.h
//  MyCheck
//
//  Created by Elad S on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@class Order;
@interface MCBusiness : NSObject {
    NSString* ID;
    NSString* Name;
    NSString* Category;
    NSInteger Distance;
    BOOL hasWaiter;
    NSString* City;
    BOOL reward;
    NSString* Address;
    BOOL quickPay;
    BOOL showBenefitsBeforeTable;
    

}
@property(strong,nonatomic)NSString*Logo;
@property(readwrite)float lat;
@property(readwrite)float lon;
@property(strong,nonatomic) NSString*html;

@property(readwrite) BOOL showBenefitsBeforeTable;

@property(readwrite)BOOL reward;
@property(readwrite)BOOL quickPay;
@property(nonatomic,strong)NSString* locationId;
-(id) initWithArr:(NSArray*)arr;
-(id) initWithDic:(NSDictionary*)arr;
-(NSString*)getAdressString;
@property(strong,nonatomic)    NSString* Address;
@property(strong,nonatomic)NSString* City;

@property(strong,nonatomic)NSString *ID;
@property(strong,nonatomic)NSString* Name;
@property(strong,nonatomic)NSString* Category;
@property(readwrite)NSInteger Distance;
@property(readwrite) BOOL hasWaiter;
-(id)initWithOrder:(Order* )order;
-(double)getDistaneFrom:(CLLocation*)other;
@end
