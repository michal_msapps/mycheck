//
//  MCCreditCardViewController.h
//  MyCheck
//
//  Created by Michal Shatz on 3/18/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"

@interface MCCreditCardViewController : MCBaseViewController
@property(nonatomic,strong)NSString* tmpPin;

@end
