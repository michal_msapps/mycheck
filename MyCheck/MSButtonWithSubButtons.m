//
//  MSButtonWithSubButtons.m
//  MSButtonWithSubButtons
//
//  Created by Michal Shatz on 3/23/15.
//  Copyright (c) 2015 MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MSButtonWithSubButtons.h"

@implementation MSButtonWithSubButtons

-(void)awakeFromNib{
    [self addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
}

-(IBAction)buttonPressed:(id)sender{
    for (UIButton *subButton in self.subButtons) {
        [subButton sendActionsForControlEvents: UIControlEventTouchUpInside];
    }
}

@end




