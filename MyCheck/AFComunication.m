//
//  AFComunication.m
//  MyCheck
//
//  Created by Elad S on 12/29/14.
//  Copyright (c) 2014 Elad S. All rights reserved.
//

#import "AFComunication.h"
#import "AFHTTPSessionManager.h"
#import <sys/utsname.h>
#import "MCBusiness.h"
#import "MCOrder.h"
#import "MCPaymentType.h"
#import "MCPaymentDetails.h"
#import "MCItemBought.h"
#import <AdSupport/AdSupport.h>
#import <CoreLocation/CoreLocation.h>
#import "AFNetworking.h"
#import "MCResponseData.h"
#import "MCRequestData.h"
#import "MyCheck.h"

#define URL_PREFIX @"https://api.mycheckapp.com"
#define URL_PREFIX_DEBUG @"https://test.mycheckapp.com"

#define MY_ERROR_DOMAIN @"my error domain"
#define BAD_FORMAT_ERROR -2
#define PAYMENT_ERROR -23
#define API_VERSION @"5"
#define NOT_LOGGED_IN_CODE 17
#define NO_OPEN_ORDER_ERROR 27

//
//  WeatherHTTPClient.m
//  Weather
//
//  Created by App-Order on 1/17/14.
//  Copyright (c) 2014 Scott Sherwood. All rights reserved.
//



@implementation AFComunication (private)

static LoginState state;
NSString* apptype;
NSString* deviceId;
BOOL isProduction;
static AFComunication *_sharedWeatherHTTPClient =nil;
@end
@implementation AFComunication
+(AFComunication*)sharedAFComunicationFirstTimeProduction:(BOOL)prod{
    isProduction=prod;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedWeatherHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:(!isProduction)?URL_PREFIX_DEBUG:URL_PREFIX]];
    });
    
    return _sharedWeatherHTTPClient;
}
+ (AFComunication *)sharedAFComunication
{
//    static AFComunication *_sharedWeatherHTTPClient = nil;
//    
//    static dispatch_once_t onceToken;
//    dispatch_once(&onceToken, ^{
//        _sharedWeatherHTTPClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:(AF_DEBUG)?URL_PREFIX_DEBUG:URL_PREFIX]];
//    });
    
    return _sharedWeatherHTTPClient;
}

- (instancetype)initWithBaseURL:(NSURL *)url
{
    self = [super initWithBaseURL:url];
    
    if (self) {
        self.responseSerializer = [AFJSONResponseSerializer serializer];
        [self.requestSerializer setValue:[self userAgentString] forHTTPHeaderField:@"User-Agent"];
        self.justEnteredPin = NO;
        self.showFailAlert = ^(MCResponseData * response, NSError *error){
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR",nil)  message: error.localizedDescription delegate:nil cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"Ok",nil), nil];
            [alert show];
        };
    }
    
    return self;
}
-(void)configWithAppType:(NSString*)appT deviceId:(NSString*)Id{
    apptype=appT;
    self.locale=@"en_GB";//(    [locale isEqualToString:@"en_GB"])?@"UK":locale;
    deviceId=Id;
    if(!deviceId)
        deviceId=@"";
    [self.requestSerializer setValue:[self userAgentString] forHTTPHeaderField:@"User-Agent"];

}

-(NSURLSessionDataTask*)sendRequest:(APICall)call withArgs:(NSDictionary*)args load:(BOOL)show
                            success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
                            failure:(void (^)(MCResponseData * response, NSError *error))failure{
    MCRequestData * req = [[MCRequestData alloc] initWithCall:call params:args method:@"POST" loader:show];
    return [self sendRequest:req success:success failure:failure];
}

//send a single request
-(NSURLSessionDataTask*) sendRequest:(MCRequestData*)req  success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
                             failure:(void (^)(MCResponseData * response, NSError *error))failure{
    
    NSString* suffix= [self APICallToSuffix:req.call];
    //    AppDelegate* appDel=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    //    [appDel showLoadingView:req.hasLoader];
    if([req.method isEqualToString:@"POST"]){
        return   [self POST:suffix parameters:req.params success:^(NSURLSessionDataTask *task, id responseObject) {
            [self handleSuccessForRequest:req response:responseObject task:task success:success failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error){
            //        [appDel showLoadingView:NO];
            
            [self handleFailureForRequest:req error:error task:task success:success failure:failure];
        }];
    }else if([req.method isEqualToString:@"GET"]){
        NSString* connectionStr = @"?";
        for(NSString* key in req.params.allKeys ){
            suffix=[suffix stringByAppendingFormat:@"%@%@=%@",connectionStr,key,[req.params objectForKey:key]];
            connectionStr=@"&";
        }
        
        return   [self POST:suffix parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
            [self handleSuccessForRequest:req response:responseObject task:task success:success failure:failure];
        } failure:^(NSURLSessionDataTask *task, NSError *error){
            //        [appDel showLoadingView:NO];
            
            [self handleFailureForRequest:req error:error task:task success:success failure:failure];
        }];
        
    }
    return nil;
}

//send multiple request with no order
-(void)sendRequests:(NSMutableArray*)requests
            success:(void (^)( NSMutableArray* responses))success
            failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableArray* tasks = [[NSMutableArray alloc]initWithCapacity:10];
    NSMutableArray* toReturn = [[NSMutableArray alloc]initWithCapacity:10];
    for(MCRequestData* req in requests){
        NSURLSessionDataTask* task =  [self sendRequest:req
                                                success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
                                                    [toReturn addObject:responseObject];
                                                    if([toReturn count] == [requests count] && success)
                                                        success(toReturn);
                                                }
                                                failure:^(MCResponseData * response, NSError *error){
                                                    if(error.code != kCFURLErrorCancelled){
                                                        if(failure)
                                                            failure(response, error);
                                                        for(NSURLSessionDataTask * tmp in tasks){
                                                            
                                                            [tmp cancel];
                                                        }
                                                        [tasks removeAllObjects];
                                                    }
                                                }
                                       ];
        
        [tasks addObject:task];
    }
    
}


//- (void)updateWeatherAtLocation:(CLLocation *)location forNumberOfDays:(NSUInteger)number
//{
//    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
//
//    parameters[@"num_of_days"] = @(number);
//    parameters[@"q"] = [NSString stringWithFormat:@"%f,%f",location.coordinate.latitude,location.coordinate.longitude];
//    parameters[@"format"] = @"json";
//    parameters[@"key"] = WorldWeatherOnlineAPIKey;
//
//    [self GET:@"weather.ashx" parameters:parameters success:^(NSURLSessionDataTask *task, id responseObject) {
//        if ([self.delegate respondsToSelector:@selector(weatherHTTPClient:didUpdateWithWeather:)]) {
//            [self.delegate weatherHTTPClient:self didUpdateWithWeather:responseObject];
//        }
//    } failure:^(NSURLSessionDataTask *task, NSError *error) {
//        if ([self.delegate respondsToSelector:@selector(weatherHTTPClient:didFailWithError:)]) {
//            [self.delegate weatherHTTPClient:self didFailWithError:error];
//        }
//    }];
//}



#pragma API call specific functions

-( NSURLSessionDataTask*)getNikoovForBenefitId:(NSString*)benID
                                       success:(void (^)( NSInteger count, NSInteger quantity))success
                                       failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSDictionary* dic =[NSDictionary dictionaryWithObject:benID forKey:@"benefit_id"];
    NSURLSessionDataTask* task=[self sendRequest:AF_GET_NIKOOV withArgs:dic load:NO success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* data = responseObject.responseJSON;
        NSInteger count= [[data objectForKey:@"Count"]intValue];
        NSInteger qunt=[[data objectForKey:@"Quantity"]intValue];
        if(success)
            success(count,qunt);
        
    }
                                         failure:failure];
    return task;
}

-(  NSURLSessionDataTask* )getBenefitsHTMLForBenefitId:(NSString*)benID chainID:(NSString*)chainID
                                               success:(void (^)( NSString* html))success
                                               failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    [dic setObject:benID forKey:@"BenefitID"];
    if(chainID)
        [dic setObject:chainID forKey:@"ChainID"];
    
    NSURLSessionDataTask* task =  [self sendRequest:AF_GET_BENEFIT_HTML withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* data = responseObject.responseJSON;
        NSString* html= [data objectForKey:@"Details"];
        if(success)
            success(html);
        
    }
                                            failure:failure];
    return task;
}

-(NSURLSessionDataTask *)getUrlsForAppType:(NSString *)appType success:(void (^)(MCResponseData * responseObject))success failure:(void (^)(MCResponseData * responseObject, NSError *error))failure{
    NSURLSessionDataTask *task;
    if(appType) {
        task = [self sendRequest:AF_GET_URLS withArgs:@{@"appType" : appType} load:YES success:^(NSURLSessionDataTask *task, MCResponseData *responseObject) {
            if (success) {
                success(responseObject);
            }
        } failure:^(MCResponseData *response, NSError *error) {
            if (failure) {
                failure(response, error);
            }
        }];
    } else {
        if (failure){
            NSDictionary *userInfo = @{NSLocalizedDescriptionKey : @"No valid app type"};
            NSError *error = [NSError errorWithDomain:nil code:400 userInfo:userInfo];
            failure(nil, error);
        }
    }
    return task;
}

-(NSURLSessionDataTask*)forgotPinForEmail:(NSString*)email Success:(void (^)(NSString* successMsg))success
                                  failure:(void (^)(MCResponseData * responseObject, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    email=[email stringByReplacingOccurrencesOfString:@" " withString:@""];
    email=[email stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    if (email) {
        [dic setObject:email forKey:@"email"];
    }
    
    //
    //    RequestData * req = [[RequestData alloc] initWithCall:AF_FORGOT params:dic method:@"POST" loader:YES];
    //    return [self sendRequest:req success:success failure:^(ResponseData* response, NSError* error){
    //        NSLog(@"%@",error.localizedDescription);
    //    }];
    NSURLSessionDataTask* task =  [self sendRequest:AF_FORGOT withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject){
        NSDictionary* response = responseObject.responseJSON;
        
        success([response objectForKey:@"successMsg"]);
    } failure:failure];
    return task;
}

-(NSURLSessionDataTask *)getAppStringsSuccess:(void (^)(MCResponseData * responseObject, NSString* successMsg))success
    failure:(void (^)(MCResponseData * responseObject, NSError *error))failure{
    NSURLSessionDataTask* task =  [self sendRequest:AF_GET_VARS_AND_STRINGS withArgs:nil load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject){
        NSDictionary* response = responseObject.responseJSON;
        success(responseObject, [response objectForKey:@"successMsg"]);
    } failure:failure];
    return task;
}

-(NSURLSessionDataTask*)getClientCodeForBusinessID:(NSString*)BID PIN:(NSString*)pin
                                           success:(void (^)( NSString* code,NSString* text,bool payFull))success
                                           failure:(void (^)(MCResponseData * response, NSError *error))failure
{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    [dic setObject:BID forKey:@"BID"];
    //if(HAS_PW_AT_OPEN_TABLE)
    if(pin)
    [dic setObject:pin forKey:@"Pass"];
    else
        [dic setObject:@true forKey:@"NoPass"];

    //    else
    //        [dic setObject:@"true" forKey:@"NoPass"];
    NSURLSessionDataTask* task =  [self sendRequest:AF_CLIENT_CODE withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        NSNumber* number=[response objectForKey:@"PayFull"];
        
        bool payFull=[number isKindOfClass:[NSNumber class]];
        if(payFull)
            payFull=[number boolValue];
        
        NSString * code=[response objectForKey:@"ReturnString"];
        // colorForNumber=[[datai  objectForKey:@"CustomerWeight"]intValue];
        NSString* text=[response objectForKey:@"ShowText" ];
        if(success)
            success(code,text,payFull);
        
    }
                                            failure:failure];
    return task;
}

-(NSURLSessionDataTask*)getNearbyBusinessWithLocation:(CLLocation*)location recent:(BOOL)recent Category:(NSString*)cat
                                              success:(void (^)( NSMutableArray* businesses,NSString* md5))success
                                              failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* args=[[NSMutableDictionary alloc]initWithCapacity:8];
    
    if(location!=nil){
        [args setObject:[NSNumber numberWithFloat:location.coordinate.latitude] forKey:@"Lat"];
        [args setObject:[NSNumber numberWithFloat:location.coordinate.longitude] forKey:@"Lon"];
        [args setObject:[NSNumber numberWithFloat:500] forKey:@"Radius"];
        
    }else{
        [args setObject:@40.71427f forKey:@"Lat"];
        [args setObject:@-74.0059700f forKey:@"Lon"];
        [args setObject:@30000.0f forKey:@"Radius"];
        
        
    }
    if(cat){
        [args setObject:cat forKey:@"Category"];
        
    }
    if(recent)
        [args setObject:@YES forKey:@"Recent"];
    if(apptype)
        [args setObject:apptype forKey:@"AppLimit"];
//    [args setObject:@20 forKey:@"ChainID" ];
    
    NSURLSessionDataTask* task =  [self sendRequest:AF_NEARBY withArgs:args load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        NSArray*arr=[response objectForKey:@"List"];
        
        NSMutableArray*businesses=[[NSMutableArray alloc] initWithCapacity:10];
        if(arr!=nil && ![arr isKindOfClass:[NSNull class]])
        {
            for(NSDictionary* dic in arr){
                
                MCBusiness* bus=[[MCBusiness alloc]initWithDic:dic];
                //                    if(selectedCat==DEAL && !bus.reward){
                //                        continue;
                //                    }
                [businesses addObject:bus];
            }
        }
        if(success)
            success(businesses,[response objectForKey:@"md5Sum"]);
        //        [businesses sortUsingComparator:^(Business* a, Business* b){
        //            double disa=[a getDistaneFrom:appDel.lastLocation];
        //            double disb=[b getDistaneFrom:appDel.lastLocation];
        //            if(disa<disb)
        //                return NSOrderedDescending;
        //            else if(disb<disa)
        //                return NSOrderedAscending;
        //            return NSOrderedSame;
        //        }];
        
        
    }
                                            failure:^(MCResponseData* response, NSError* error){
                                                failure(response,error);
                                            }];
    return task;
    
}

//get the order details for a specific order ID
//if orderID is nil the function will return the order that is currently open or fail.
-(NSURLSessionDataTask*)getOrderDetailsForOrderID:(NSString*)orderID tableCode:(NSString*)code polling:(BOOL)polling
                                          success:(void (^)( MCOrder* order))success
                                          failure:(void (^)(MCResponseData * task, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    if(orderID)
        [dic setObject:orderID forKey:@"OrderID"];
    if(code)
        [dic setObject:code forKey:@"Code"];
    
    [dic setObject:[NSNumber numberWithBool:polling] forKey:@"Quick"];
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_VIEW_ORDER withArgs:dic load:NO success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        
        
        
        NSString* PaymentError=[response objectForKey:@"PaymentError"];
        if(PaymentError && PaymentError.length>0){
            NSString* text= PaymentError;
            NSInteger code = PAYMENT_ERROR;
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:text forKey:NSLocalizedDescriptionKey];
            NSError* error = [NSError errorWithDomain:MY_ERROR_DOMAIN code:code  userInfo:details];
            MCRequestData * req = [[MCRequestData alloc] initWithCall:AF_VIEW_ORDER params:dic method:@"POST" loader:NO];
            
            MCResponseData* res = [[MCResponseData alloc]initWithRequest:req json:nil error:error];
            
            failure(res, error);
        }else{
            MCOrder* order=[[MCOrder alloc] initWithDic:response];
            if(success)
                success(order);
            return ;
        }
        
        
    }
                      failure:^(MCResponseData *res, NSError *error){
                          if(error.code == 228){
                              NSString* errorTxt= [res.responseJSON objectForKey:@"OpenError"];
                              if(!errorTxt)
                                  errorTxt=@"";
                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                              
                              [details setValue:errorTxt forKey:NSLocalizedDescriptionKey];
                              
                              NSError* error2 = [NSError errorWithDomain:MY_ERROR_DOMAIN code:error.code userInfo:details];
                              failure(res,error2);
                          }
                          else
                              failure(res,error);
                      }
             ];
    return task;
}
-(NSURLSessionDataTask*)isTableOpenSuccess:(void (^)(BOOL open, NSString* locationId))success
                                   failure:(void (^)(MCResponseData * task, NSError *error))failure{
    return [self getOrderDetailsForOrderID:nil tableCode:nil polling:NO success:^(MCOrder* order){
        success(YES,order.locationId);
    } failure:^(MCResponseData* task, NSError* error){
        if(error.code == NO_OPEN_ORDER_ERROR){
            success(NO,nil);
        }else{
            failure(task,error);
        }
    }];
}


-(NSURLSessionDataTask*)getPaymentMethodsWithBusinessId:(NSString*)bid orderId:(NSString*)orderId success:(void (^)( NSMutableArray* methods))success
                                                failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    if(orderId)
        [dic setObject:orderId forKey:@"OrderID"];
    if(bid)
        [dic setObject:bid forKey:@"BID"];
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_PAYMENT_METHOD withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        NSArray* list=[response objectForKey:@"List"];
        
        NSMutableArray* paymentMethods = [[NSMutableArray alloc] initWithCapacity:10];
        for(NSDictionary* dic in list){
            MCPaymentType* tmp = [[MCPaymentType alloc] initWithDic:dic];
            [paymentMethods addObject:tmp];
            
            
        }
        if(success)
            success(paymentMethods);
        
        
        
    }
                      failure:^(MCResponseData *res, NSError *error){
                          if(error.code == 228){
                              NSString* errorTxt= [res.responseJSON objectForKey:@"OpenError"];
                              if(!errorTxt)
                                  errorTxt=@"";
                              NSMutableDictionary* details = [NSMutableDictionary dictionary];
                              
                              [details setValue:errorTxt forKey:NSLocalizedDescriptionKey];
                              
                              NSError* error2 = [NSError errorWithDomain:MY_ERROR_DOMAIN code:error.code userInfo:details];
                              failure(res,error2);
                          }
                          else
                              failure(res,error);
                      }
             ];
    return task;
    
}

-(NSURLSessionDataTask*)setPaymentMethodasDefaultWithType:(NSString*)type cardId:(NSString*)cardId success:(void (^)())success
                                                  failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    if(type)
        [dic setObject:type forKey:@"PrefPaymentMethod"];
    if(cardId)
        [dic setObject:cardId forKey:@"CardID"];
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_CHANGE_PAYMENT_METHOD withArgs:dic load:YES success:success failure:failure ];
    return task;
    
}

-(NSURLSessionDataTask*)deletePaymentMethodWithId:(NSString*)Id isPaypal:(BOOL)paypal success:(void (^)())success
                                          failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    if(paypal)
        [dic setObject:Id forKey:@"PayPalID"];
    else
        [dic setObject:Id forKey:@"CardID"];
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_DELETE_PAYMENT_METHOD withArgs:dic load:YES success:success failure:failure ];
    return task;
}

-(NSURLSessionDataTask*)makePayment:(MCPaymentDetails*)paymentDetails  success:(void (^)(NSString* paymentId, BOOL askForFeedback))success
                            failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    [dic setObject:paymentDetails.order.OrderID forKey:@"OrderID"];
    [dic setObject:[NSString stringWithFormat:@"%.2f",paymentDetails.paymentAmount] forKey:@"ItemAmount"];
    [dic setObject:[NSString stringWithFormat:@"%.2f",paymentDetails.serviceAmount] forKey:@"ServiceAmount"];
    if(paymentDetails.paymentMethod){
        [dic setObject:paymentDetails.paymentMethod.Type forKey:@"MethodType"];
        [dic setObject:paymentDetails.paymentMethod.ID forKey:@"MethodID"];
    }
    if(paymentDetails.applePayToken){
        [dic setObject:paymentDetails.applePayToken forKey:@"appleToken"];
        [dic setObject:paymentDetails.applePayCardType forKey:@"cardType"];
        
    }
    [dic setObject:[NSNumber numberWithBool:paymentDetails.sendInvoice] forKey:@"SendInvoice"];
    [dic setObject:[self createItemsJSONStringFromItems:paymentDetails.items] forKey:@"Items"];
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_PAYMENT withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        NSString* paymentId = [response objectForKey:@"ReturnString"];
        NSNumber* num=[response objectForKey:@"PopUpFeedback"];
        BOOL feedback=[num boolValue];
        [self checkForPaymentComplition:paymentId success:^(){
            success(paymentId,feedback);
        } failure:failure];
        
    }
                      failure:failure ];
    return task;
}
-(NSURLSessionDataTask*)registerWithName:(NSString*)name surname:(NSString*)sname email:(NSString*)email pin:(NSString*)pin phone:(NSString*)phone dob:(NSString*)dob pushToken:(NSString*)token FBToken:(NSString*)fb success:(void (^)())success
                                 failure:(void (^)(MCResponseData * response, NSError *error))failure{
    
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    [dic setObject:name forKey:@"FName"];
    [dic setObject:sname forKey:@"LName"];
    [dic setObject:email forKey:@"Username"];
    [dic setObject:pin forKey:@"Pass"];
    if(phone)
        [dic setObject:phone forKey:@"Phone"];
    if(dob)
        [dic setObject:dob forKey:@"DOB"];
    if(token)
        [dic setObject:token forKey:@"dToken"];
    if(fb){
        [dic setObject:@YES forKey:@"byFacebook"];
        [dic setObject:fb forKey:@"facebookToken"];
    }
    [dic setObject:apptype forKey:@"apptype"];
    NSString* loc = [self.locale isEqualToString:@"UK"]?@"en_GB":self.locale;
    [dic setObject:loc forKey:@"Locale"];

    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_REGISTRATION withArgs:dic load:YES success:success failure:failure];
    return task;
    
}
-(NSURLSessionDataTask*)loginWithEmail:(NSString*)email pin:(NSString*)pin pushToken:(NSString*)token FBToken:(NSString*)fb success:(void (^)(BOOL hasPaymentMethod))success
                               failure:(void (^)(MCResponseData * response, NSError *error))failure{
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    [dic setObject:email forKey:@"Username"];
    [dic setObject:pin forKey:@"Pass"];
    if(token)
        [dic setObject:token forKey:@"dToken"];
    if(fb){
        [dic setObject:@YES forKey:@"byFacebook"];
        [dic setObject:fb forKey:@"facebookToken"];
    }
    [dic setObject:apptype forKey:@"apptype"];
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_LOGIN withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData * responseObject){
      bool  hasPMethod= [[responseObject.responseJSON objectForKey:@"PaymentMethods"] boolValue];
        success(hasPMethod);
    }
              failure:failure];
    return task;
}

-(NSURLSessionDataTask*)logoutSuccess:(void (^)())success
                              failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_LOGOUT withArgs:dic load:YES success:success failure:failure];
    return task;
}
-(NSURLSessionDataTask*)checkForPaymentComplition:(NSString*)paymentID  success:(void (^)())success
                                          failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSDate* lastmsgSentDate=[NSDate date];
    
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    [dic setObject:paymentID forKey:@"PaymentID"];
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_PAYENT_STATUS withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData* responseObject) {
        NSDictionary* response = responseObject.responseJSON;
        NSString* status = [response objectForKey:@"ReturnString"];
        if([status isEqualToString:@"Pending"]){
            float time=4-[[NSDate date] timeIntervalSinceDate:lastmsgSentDate] ;
            if(time<0)
                time=0.001;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, time * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [self checkForPaymentComplition:paymentID success:success failure:failure];
            });
        }else  if([status isEqualToString:@"Approved"]){
            success();
        }else{
            NSLog(@"ASERT PROBLEM");
        }
        
        
        
    }
                      failure:failure];
    return task;
    
}


-(NSURLSessionDataTask*)AddCreditCardWithNumber:(NSString*)number expiration:(NSString*)date cvv:(NSString*)cvv zip:(NSString*)zip success:(void (^)())success
                                        failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_ADD_CREDIT_CARD withArgs:dic load:YES success:^(NSURLSessionDataTask *task, MCResponseData * responseObject){
        NSDictionary* response = responseObject.responseJSON;
        NSString* token =[response objectForKey:@"token"];
        NSString* processorId = [response objectForKey:@"processorId"];
        NSString* vendorId = [response objectForKey:@"vendorId"];
        NSString* callback = [response objectForKey:@"callback"];
        NSString*transactionId = [response objectForKey:@"transactionId"];
        NSString* clientRef = [response objectForKeyedSubscript:@"clientRef"];
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSMutableDictionary* params = [[NSMutableDictionary alloc]initWithCapacity:2];
        NSString* url = !isProduction?@"https://devpm.mycheckapp.com:8443/PaymentManager/InsertCreditCard":
        @"https://pm.mycheckapp.com:8443/PaymentManager/InsertCreditCard";
        
        [params setObject:token forKey:@"token"];
        [params setObject:processorId forKey:@"processorId"];
        [params setObject:vendorId forKey:@"vendorId"];
        [params setObject:callback forKey:@"callback_url"];
        [params setObject:number forKey:@"CreditDetails[rawNumber]"];
        NSString* month =[date substringToIndex:2];
        NSString* year = [@"20" stringByAppendingString:[date substringFromIndex:3]];
        [params setObject:month forKey:@"CreditDetails[expireMonth]"];
        [params setObject:year forKey:@"CreditDetails[expireYear]"];
        [params setObject:zip forKey:@"CreditDetails[postalCode]"];
        [params setObject:cvv forKey:@"CreditDetails[cvc]"];
        [params setObject:clientRef forKey:@"clientRef"];
        [params setObject:transactionId forKey:@"transactionid"];
        [params setObject:[self cardType:number] forKey:@"CreditDetails[type]"];
        [manager.requestSerializer setValue:token forHTTPHeaderField:@"token"];
        MCRequestData* req = [[MCRequestData alloc] initWithCall:AF_ADD_CREDIT_CARD params:params method:@"POST" loader:YES];
        [manager POST:url parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            //  NSLog(@"JSON: %@", responseObject);
            [self handleSuccessForRequest:req response:responseObject task:task success:^(NSURLSessionDataTask *task, MCResponseData * responseObject){
                success();
            } failure:failure];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //  NSLog(@"Error: %@", error);
            MCResponseData* res = [[MCResponseData alloc]initWithRequest:req json:nil error:error];
            
            failure(res,error);
        }];
    } failure:failure];
    return task;
    
}

-(LoginState)isLoggedInSuccess:(void (^)(BOOL loggedIn,BOOL hasPaymentMethod))success
                       failure:(void (^)(MCResponseData * response, NSError *error))failure{
    //if we already know if the user is logged in or not we return imidiatly
    if(state != UNKNOWN_LOGIN_STATE){
        success(state == LOGGEDIN,[self hasPaymentMethod]);
        return state;
    }
    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    
    
    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_GET_USER_DETAILS withArgs:dic load:YES success:^(NSURLSessionDataTask* data, MCResponseData* response){
        state = LOGGEDIN;
      bool  hasPMethod= [[response.responseJSON objectForKey:@"PaymentMethods"]boolValue];
        if(success)
            success(YES,hasPMethod);
    } failure:^(MCResponseData* data,NSError* error){
        if(error.code ==NOT_LOGGED_IN_CODE){
            state = LOGDOUT;
        // bool   hasPMethod=NO;
            if(success)
                success(NO,NO);
        }else{
            if(failure)
                failure(data,error);
        }
    }];
    return state;
}

-(NSURLSessionDataTask*)authenticatePayPalWithAuthCode:(NSString*)auth locationId:(NSString*)locationId metaDataId:(NSString*) metadataId success:(void (^)())success
                                               failure:(void (^)(MCResponseData * response, NSError *error))failure{

    NSMutableDictionary* dic = [[NSMutableDictionary alloc]initWithCapacity:2];
    NSString* loc = [self.locale isEqualToString:@"UK"]?@"en_GB":self.locale;
    [dic setObject:loc forKey:@"locale"];
    [dic setObject:auth forKey:@"authcode"];
    [dic setObject:metadataId forKey:@"metadataId"];

    if(locationId)
        [dic setObject:locationId forKey:@"locationId"];

    NSURLSessionDataTask* task = nil;
    task =  [self sendRequest:AF_AUTHENTICATE_PAYPAL withArgs:dic load:YES success:^(NSURLSessionDataTask *task,MCResponseData* response){
        success();
    } failure:failure];
    return task;

    
}
#pragma PRIVATE METHODS


-(NSString*)createItemsJSONStringFromItems:(NSMutableArray*)items{
    NSMutableArray* array = [[NSMutableArray alloc]initWithCapacity:10];
    for(ItemBought* item in items){
        
        NSDictionary*dest=[NSDictionary dictionaryWithObjectsAndKeys:item.ID,@"ID",@"1",@"Amnt", nil];
        [array addObject:dest];
    }
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array
                                                       options:0 // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        
    } else {
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        return jsonString;
    }
    
    return nil;
}

-(NSString*)userAgentString{
    NSString* device=[self machineName ];
    NSString* toReturn=[UIDevice currentDevice].model;
    //[NSString stringWithFormat:@"%@", NSLocalizedString(@"LOCALE", nil) ];;
    NSString* applePay=[NSString stringWithFormat:@"AppleP%@",@"False"];
    NSString *adId = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    
    toReturn =[toReturn stringByAppendingFormat:@" [%@][%@][%@][%@][%@][%@][%@][%@]",[[UIDevice currentDevice] systemVersion],API_VERSION,self.locale,deviceId,adId,device,apptype,applePay];
    //    NSLog(@"identifierForVendor %@",[[UIDevice currentDevice] identifierForVendor]);
    //    NSLog(@"UAKeychainUtils %@",[UAKeychainUtils getDeviceID]);
    toReturn = [toReturn stringByReplacingOccurrencesOfString:@"iPad" withString:@"iPhone"];
    return toReturn;
}

-(NSString*)APICallToSuffix:(APICall)call{
    NSString* suffix =nil;
    switch (call)
    {
        case AF_LOGIN:
            suffix=@"/mobileApi/login";
            break;
        case AF_CHANGE_PAYMENT_METHOD:
            suffix=@"/mobileApi/updatePrefMethod";
            break;
        case AF_LIST_FOR_GIFT_CARD:
            suffix=@"/mobileApi/giftCard/";
            break;
        case AF_BUY_GIFT_CARD:
            suffix=@"/mobileApi/giftCard/act/buy";
            break;
        case AF_SEND_GIFT_CARD_TO_FRIEND:
            suffix=@"/mobileApi/giftCard/act/gift";
            break;
        case AF_PAYMENT_METHOD:
            suffix=@"/mobileApi/paymentMethods";
            
            break;
        case AF_IS_BENEFIT_VALID:
            suffix=@"/mobileApi/IsBenefitMatchConditions";
            
            break;
        case AF_GET_PLACE_NOTIFICATION:
            suffix=@"/mobileApi/getLoyaltyMessage";
            
            break;
        case AF_GET_VARS_AND_STRINGS:
            suffix=@"/mobileApi/appStrings";
            
            break;
        case AF_REGISTER_NOTIFICATION:
            suffix=@"/mobileApi/RegisterLoyalty";
            
            break;
        case AF_GET_OFFLINE_BIZLIST:
            suffix=@"/mobileApi/getPlaces";
            
            break;
        case AF_GET_OFFLINE_BENEFIT_LIST:
            suffix=@"/mobileApi/BenfitListAll";
            
            break;
        case AF_REGISTER_BENEFIT:
            suffix=@"/mobileApi/registerBenefitQuick";
            
            break;
        case AF_CANCEL_BENEFIT:
            suffix=@"/mobileApi/cancelBenefitQuick";
            
            break;
        case AF_BENEFIT_COUNT:
            suffix=@"/mobileApi/TotalBenfit";
            
            break;
        case AF_ADD_USER_TO_TABLE:
            suffix=@"/mobileApi/joinCode";
            
            break;
        case AF_PROMO_CODE:
            suffix=@"/mobileApi/pilotCode";
            
            break;
            
        case AF_SEND_RECIPT_IN_EMAIL:
            suffix=@"/mobileApi/invoice";
            
            break;
        case AF_POST_PAYMENT:
            suffix=@"/mobileApi/postPayment";
            
            break;
        case AF_GET_COUPON_DETAILS:
            suffix=@"/mobileApi/generalGC";
            
            break;
        case AF_DELETE_PAYMENT_METHOD:
            suffix=@"/mobileApi/delCredit";
            break;
        case AF_SET_USER:
            suffix=@"/mobileApi/userDetails";
            
            
            break;
        case AF_REGISTRATION:
            suffix=@"/mobileApi/register";
            
            break;
        case AF_GET_PLACE_INFO:
            suffix=@"/mobileApi/bizinfo";
            break;
        case AF_GET_BENEFIT_HTML:
            suffix=@"/mobileApi/getBenefitHTML";
            break;
        case AF_GET_INVITATION_MSG:
            suffix=@"/mobileApi/postinvitations";
            break;
        case AF_SEND_FEEDBACK:
            suffix=@"/mobileApi/feedback";
            break;
        case AF_GET_BAlANCE:
            suffix=@"/mobileApi/userbalance";
            break;
        case AF_PAS_TO_PIN:
            suffix=@"/mobileApi/passtopin";
            break;
        case AF_POPUP_BENEFIT:
            suffix=@"/mobileApi/setInstant";
            break;
        case AF_GET_GAS_NUMBER:
            suffix=@"/mobileApi/canFuel";
            break;
        case AF_PAYMENT:
            suffix=@"/mobileApi/payment";
            break;
        case AF_GET_MEMBER_LIST:
            suffix=@"/mobileApi/memberList";
            break;
        case AF_UPDATE_PAYED_ITEMS:
            
            suffix=@"/mobileApi/UpdatePaidItems";
            break;
        case AF_BIZ_LIST_CITY:
            
            suffix=@"/mobileApi/bizListCity";
            break;
        case AF_VIEW_ORDER :
            suffix=@"/mobileApi/orderDetails";
            
            break;
        case AF_GET_URLS:
            suffix=@"/mobileApi/getPageContent";
            break;
        case AF_CLIENT_CODE:
            suffix=@"/mobileApi/getCode";
            break;
        case AF_GET_NIKOOV:
            suffix=@"/mobileApi/getbenfitnikovim";
            break;
        case AF_VALIDATE_ZIP:
            suffix=@"/mobileApi/validateAddressForDelivery";
            break;
        case AF_REWARD_LIST:
            suffix=@"/mobileApi/rewardsList";
            break;
        case AF_PAYENT_STATUS:
            suffix=@"/mobileApi/paymentStatus";
            
            break;
        case AF_LOGOUT:
            suffix=@"/mobileApi/logout";
            
            break;
        case AF_FORGOT:
            suffix=@"/site/GetPass";
            
            break;
        case AF_NEARBY:
            suffix=@"/mobileApi/bizList";
            break;
        case AF_ADD_CREDIT_CARD:
            suffix=@"/mobileApi/GetAcceesTokenMPI";
            break;
        case AF_GET_USER_DETAILS:
            suffix = @"/mobileApi/userDetails";
            break;
        case AF_AUTHENTICATE_PAYPAL:
            suffix = @"WhiteLabel/SetPaypalUserToken";
            break;
        default:
            NSLog(@"no such request in -(void)sendRequest:(NSInteger)reqID withArgs:(NSArray*)args    requestID:");
            break;
    }
    return suffix;
}
-(NSString*) machineName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine
                              encoding:NSUTF8StringEncoding];
}

-(void)handleSuccessForRequest:(MCRequestData*)req response:(NSDictionary*) response task:(NSURLSessionDataTask*)task success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
                       failure:(void (^)(MCResponseData * response, NSError *error))failure{
    NSError* error = nil; //will not be nil if we need to send a failure response
    if(![response isKindOfClass:[NSDictionary class]]){
        //bad format of response
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"Bad formated response" forKey:NSLocalizedDescriptionKey];
        error = [NSError errorWithDomain:MY_ERROR_DOMAIN code:BAD_FORMAT_ERROR userInfo:details];
    }else{
        //checking for a error from the server
        NSInteger errorCode=BAD_FORMAT_ERROR;
        NSString* tmp=[response objectForKey:@"ErrorCode"] ;
        
        if(tmp!=nil){
            errorCode=[tmp intValue];
        }
        if(req.call == AF_FORGOT){
            NSNumber* succ= [response objectForKey:@"success"];
            errorCode = ([succ isKindOfClass:[NSNumber class]] && [succ intValue]==1)?0:-43;
        }
        if(errorCode!=0){
            //if it is forgot password message and it failed...
            NSString * returnS=(errorCode==-876)?[response objectForKey:@"error"] :[response objectForKey:@"ReturnString"];
            
            //bad format of response
            NSMutableDictionary* details = [NSMutableDictionary dictionary];
            [details setValue:returnS forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:MY_ERROR_DOMAIN code:errorCode userInfo:details];
        }
        
        
    }
    if(error){
        
        //updating loggin state if needed
        if(error.code == NOT_LOGGED_IN_CODE)
            state = LOGDOUT;
        else if (req.call == AF_LOGOUT)
            state = UNKNOWN_LOGIN_STATE;
        MCResponseData* res = [[MCResponseData alloc]initWithRequest:req json:nil error:error];
        if(failure)
            failure(res, error);
    }
    else{
        //            [appDel showLoadingView:NO];
        if(req.call == AF_LOGIN)
            state = LOGGEDIN;
        else if (req.call == AF_LOGOUT)
            state = LOGDOUT;
        MCResponseData* res = [[MCResponseData alloc]initWithRequest:req json:response error:nil];
        if(success)
            success(task,res);
    }
}

-(void)handleFailureForRequest:(MCRequestData*)req error:(NSError*) error task:(NSURLSessionDataTask*)task success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
                       failure:(void (^)(MCResponseData * response, NSError *error))failure{
    MCResponseData* res = [[MCResponseData alloc]initWithRequest:req json:nil error:error];
    if(![error.domain isEqualToString:MY_ERROR_DOMAIN]){
        if(error.code == kCFURLErrorCancelled){
            return ;
        }
        NSLog(@"AFComunication error: %@",error.localizedDescription);
        NSMutableDictionary* details = [NSMutableDictionary dictionary];
        [details setValue:@"The Internet connection appears to be offline." forKey:NSLocalizedDescriptionKey];
        
        error = [NSError errorWithDomain:MY_ERROR_DOMAIN code:BAD_FORMAT_ERROR userInfo:details];
    }
    if(failure)
        failure(res, error);
    
}

- (NSString*)cardType:(NSString*)number
{
    if (number.length < 2) {
        return @"";
    }
    
    NSString *firstChars = [number substringWithRange:NSMakeRange(0, 2)];
    NSInteger range = [firstChars integerValue];
    
    if (range >= 40 && range <= 49) {
        return @"Visa";
    } else if (range >= 50 && range <= 59) {
        return @"Master Card";
    } else if (range == 34 || range == 37) {
        return @"American Express";
    } else if (range == 60 || range == 62 || range == 64 || range == 65) {
        return @"Discover";
    } else if (range == 35) {
        return @"JCB";
    } else if (range == 30 || range == 36 || range == 38 || range == 39) {
        return @"Diners";
    } else {
        return @"";
    }
    return @"";
    
}
- (NSString*)getURLPrefixString{
    return (!isProduction)?URL_PREFIX_DEBUG:URL_PREFIX;
}
-(bool)hasPaymentMethod{
   return [[[MyCheck sharedInstance].userDefaults valueForKey:@"hasPaymentMethod"] boolValue];
}
-(void)setHasPaymentMethod:(bool)has{
    [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:has] forKey:@"hasPaymentMethod"];
}
@end
