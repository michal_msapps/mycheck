//
//  MyCheck.m
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MyCheck.h"
#import "MCPaymentMethodViewController.h"
#import "MCNavigationController.h"
#import "MCUserDefaults.h"
#import "MCResponseData.h"
#import "UIViewController+Transitions.h"
#import "NSBundle+MCBundle.h"
#import "MCWebViewController.h"

@interface MyCheck ()
{
}
@property (nonatomic, strong) NSString *deviceId;

@end

@implementation MyCheck

@synthesize locationId;

+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL) prod{
    [[AFComunication sharedAFComunicationFirstTimeProduction:prod] configWithAppType:appType  deviceId:deviceId];
    [self installForAppType:appType ];
    [MyCheck sharedInstance].deviceId = deviceId;
    [MyCheck sharedInstance].userDefaults = [[MCUserDefaults alloc] init];
    [[MyCheck sharedInstance] setStrings];
}

+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL) prod logAppEventBlock:(logAppEventBlock)logAppEventBlock{
    [MyCheck installForAppType:appType deviceId:deviceId productionServer:prod];
    [MyCheck sharedInstance].appEventBlock = logAppEventBlock;
}

+(void)installForAppType:(NSString*)appType deviceId:(NSString *)deviceId productionServer:(BOOL)prod logAppScreenView:()logAppScreenView logAppEventBlock:(logAppEventBlock)logAppEventBlock{
    [self installForAppType:appType deviceId:deviceId productionServer:prod logAppEventBlock:logAppEventBlock];
}

+(void)installForAppType:(NSString*)appType {
    [NSBundle mycheckBundle];
    [[AFComunication sharedAFComunication] isLoggedInSuccess:^(BOOL loggedIn,BOOL hasPaymentMethod) {
        [MyCheck sharedInstance].loggedIn = loggedIn;
        [AFComunication sharedAFComunication].hasPaymentMethod = hasPaymentMethod;
        [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:hasPaymentMethod] forKey:@"hasPaymentMethod"];
    } failure:^(MCResponseData *response, NSError *error) {
        [MyCheck sharedInstance].loggedIn = NO;
    }];
    
    [[AFComunication sharedAFComunication] getUrlsForAppType:appType success:^(MCResponseData *responseObject) {
        NSDictionary *contentDic = [responseObject.responseJSON objectForKey:@"Content"];
        [[MyCheck sharedInstance].userDefaults setValue:[contentDic objectForKey:@"Terms and Conditions"] forKey:@"Terms and Conditions url"];
    } failure:^(MCResponseData *responseObject, NSError *error) {
        
    }];
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"AXbSaQpAjBisS0qVVxPV0cqqOdyxQF_W_2AOTz4UkHIXI1AMKll--Z0LtZa7TEGxuE847m_2wziDcQxk"}];
    [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
}

+(instancetype)sharedInstance{
    static MyCheck *_sharedClient = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedClient = [[MyCheck alloc] init];
    });
    return _sharedClient;
}

-(void)showMyCheck:(UIViewController *)viewController{

    UIColor* color =[UITextField appearance].tintColor;
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    [[MyCheck sharedInstance].userDefaults setValue:colorData forKey:@"originalTextFieldTint"];
    [[UITextField appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
    [[UIButton appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
    [[UIAlertView appearance] setTintColor:[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1]];
    [[AFComunication sharedAFComunication] isLoggedInSuccess:^(BOOL loggedIn,BOOL hasPaymentMethod) {
        UIViewController *nextViewController;
        if ([AFComunication sharedAFComunication].openTable && loggedIn) {
            [AFComunication sharedAFComunication].justEnteredPin = NO;
            nextViewController = [[MCWebViewController alloc] initWithNibName:@"MCWebViewController" bundle:[NSBundle mycheckBundle]];
        } else {
            nextViewController = [[MCPaymentMethodViewController alloc] initWithNibName:@"MCPaymentMethodView" bundle:[NSBundle mycheckBundle]];
        }
        nextViewController.modalPresentationStyle = UIModalPresentationFormSheet;
        nextViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        UINavigationController *mcNavigationController = [[UINavigationController alloc] initWithRootViewController:nextViewController];
        [viewController presentViewController:mcNavigationController withPushAnimationAndCompletion:nil];
    } failure:nil];
    
    
   
}

-(void)setStrings{
    [[AFComunication sharedAFComunication] getAppStringsSuccess:^(MCResponseData *responseObject, NSString *successMsg) {
        NSDictionary* appStringsDictionary = responseObject.responseJSON;
        [[MyCheck sharedInstance].userDefaults setValue:[appStringsDictionary objectForKey:@"BookPayment1"] forKey:@"BookPayment1"];
        [[MyCheck sharedInstance].userDefaults setValue:[appStringsDictionary objectForKey:@"BookPayment2"] forKey:@"BookPayment2"];

    } failure:^(MCResponseData *responseObject, NSError *error) {
        
    }];
}


#pragma AFComunicationRapperFunctions

-(void)isTableOpenSuccess:(void (^)(BOOL open, NSString* locationId))success
                  failure:(void (^)(MCResponseData * task, NSError *error))failure{
//    [[AFComunication sharedAFComunication] isTableOpenSuccess:success failure:failure];
    [[AFComunication sharedAFComunication] isTableOpenSuccess:^(BOOL open, NSString *locId ) {
        
        [AFComunication sharedAFComunication].openTable=open;
        if (success) {
            success(open, locId);
        }
    } failure:^(MCResponseData *task, NSError *error) {
        failure (task, error);
        [AFComunication sharedAFComunication].openTable=NO;

    }];
}

-(void)getNearbyBusinessWithLocation:(CLLocation*)location success:(void (^)( NSMutableArray* businesses))success
                             failure:(void (^)(MCResponseData * response, NSError *error))failure{
    [[AFComunication sharedAFComunication]  getNearbyBusinessWithLocation:location recent:NO Category:nil success:^(NSMutableArray* businesses,NSString* md5){
        success(businesses);
    } failure:failure];
}

-(void)isLoggedInSuccess:(void (^)(BOOL loggedIn))success
                 failure:(void (^)(MCResponseData * response, NSError *error))failure{
    [[AFComunication sharedAFComunication] isLoggedInSuccess:^(BOOL loggedIn,BOOL hasPaymentMethod) {
        [MyCheck sharedInstance].loggedIn = loggedIn;
        [AFComunication sharedAFComunication].hasPaymentMethod = hasPaymentMethod;
        [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:hasPaymentMethod] forKey:@"hasPaymentMethod"];
        if(!loggedIn)
            [[MyCheck sharedInstance].userDefaults removeValueForKey:@"userEmail"];
        success(loggedIn);
    } failure:failure];
}

-(void)logoutSuccess:(void (^)())success
             failure:(void (^)(MCResponseData * response, NSError *error))failure{
    
    [[AFComunication sharedAFComunication] logoutSuccess:^{
        success();
        [MyCheck sharedInstance].loggedIn = NO;
        [[MyCheck sharedInstance].userDefaults removeValueForKey:@"userEmail"];
        
    } failure:^(MCResponseData * response, NSError *error){
        if(error.code == NOT_LOGGED_IN_CODE){
            [MyCheck sharedInstance].loggedIn = NO;
            [[MyCheck sharedInstance].userDefaults removeValueForKey:@"userEmail"];
        }
        failure(response,error);
    }];
    
    
}

@end