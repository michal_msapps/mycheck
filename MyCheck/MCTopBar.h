//
//  MCTopBar.h
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCTopBarDelegate <NSObject>

-(IBAction)back:(id)sender;
-(void)loggedOut;
-(void)failedToLogout;

@end

@interface MCTopBar : UIView

@property (nonatomic, strong) IBOutlet UIButton *backButton;
@property (nonatomic, strong) IBOutlet UILabel *lblTitle;

@property (nonatomic, weak) id<MCTopBarDelegate> delegate;

-(IBAction)back:(id)sender;

@end
