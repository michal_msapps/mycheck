//
//  UIColor+MCColor.m
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "UIColor+MCColor.h"

@implementation UIColor (MCColor)

+(UIColor *)myCheckMagento{
    return [UIColor colorWithRed:87/255 green:129/255 blue:237/255 alpha:1];
}

@end
