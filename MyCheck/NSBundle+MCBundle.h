//
//  NSBundle+MCBundle.h
//  MyCheck
//
//  Created by Michal Shatz on 4/9/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSBundle (MCBundle)

+ (NSBundle *)mycheckBundle;
-(UIImage *)imageName:(NSString *)imageName;

@end
