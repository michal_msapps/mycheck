//
//  MCCreditCardViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 3/18/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCCreditCardViewController.h"
#import "PTKView.h"
#import "MCWebViewController.h"
#import "AFComunication.h"
#import "NSBundle+MCBundle.h"
#import "MCPaymentMethodViewController.h"

@interface MCCreditCardViewController () <PTKViewDelegate>

@property (nonatomic, strong) IBOutlet PTKView *paymentView;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint *constraintTopPaymentView;

@end

@implementation MCCreditCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.paymentView.delegate = self;
    self.topBar.lblTitle.text = @"Payment";
    if (self.paymentView.frame.origin.y > self.view.frame.size.height / 3) {
        self.paymentView.frame = CGRectMake(self.paymentView.frame.origin
                                                    .x, self.view.frame.size.height / 4, self.paymentView.frame.size.width, self.paymentView.frame.size.height);
    }
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *)padDateIfNeeded:(NSString *)dateString{
    NSRange rangeOfDash = [dateString rangeOfString:@"/"];
    NSString *monthString = [dateString substringToIndex:rangeOfDash.location];
    if (monthString.length < 2) {
        return [NSString stringWithFormat:@"0%@", dateString];
    } else {
        return dateString;
    }
}

-(IBAction)skip:(id)sender{
    [self segueToWebviewController];
}

-(void)segueToWebviewController{
    MCWebViewController *mcWebViewController = [[MCWebViewController alloc] initWithNibName:@"MCWebViewController" bundle:[NSBundle mycheckBundle]];
    mcWebViewController.tmpPin=self.tmpPin;
    self.tmpPin=nil;
    [self.navigationController pushViewController:mcWebViewController animated:YES];
}

//- (void)registerForKeyboardNotifications{
//    return;
//}

-(IBAction)back:(id)sender{
    [self showNoPaymentAlert];
}

#pragma mark PTKViewDelegate

-(void)paymentView:(PTKView *)paymentView withCard:(PTKCard *)card isValid:(BOOL)valid{
    if (valid) {
        NSString *expirationDate = [NSString stringWithFormat:@"%lu/%lu", (unsigned long)card.expMonth, (unsigned long)card.expYear % 100];
        __block UIActivityIndicatorView *activityIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activityIndicatorView.center = self.view.center;
        [self.view addSubview:activityIndicatorView];
        [activityIndicatorView startAnimating];
        [[AFComunication sharedAFComunication] AddCreditCardWithNumber:card.number expiration:[self padDateIfNeeded:expirationDate] cvv:card.cvc zip:card.addressZip success:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [activityIndicatorView removeFromSuperview];
                [self segueToWebviewController];
                [AFComunication sharedAFComunication].hasPaymentMethod = YES;
            });
        } failure:^(MCResponseData *response, NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [activityIndicatorView stopAnimating];
                [activityIndicatorView removeFromSuperview];
            });
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Bad Card", @"") message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
        }];
    } else {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Bad credit card" message:@"Please try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
        [self.paymentView clearViews];
    }
}

#pragma mark Keyboard Notifications Methods

- (void)keyboardWasShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
  
    CGRect aRect = [UIScreen mainScreen].bounds;
    aRect.size.height -= kbSize.height;
    
    CGRect bkgndRect = self.activeField.superview.frame;
    bkgndRect.size.height += kbSize.height;
    CGPoint visiblePoint = self.paymentView.frame.origin;
    visiblePoint.y += self.paymentView.frame.size.height;
    if (!CGRectContainsPoint(aRect, visiblePoint)) {
        self.constraintTopPaymentView.constant = (self.view.frame.size.height-kbSize.height)/2- 20 ;
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
