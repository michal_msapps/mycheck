//
//  MCWebViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 3/15/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCWebViewController.h"
#import "MyCheck.h"
#import "NSString+Category.h"
#import "AFComunication.h"
#import "MCAnalyticsStrings.h"
#define ERROR_CANCEL 101
#define NAME_OF_SCREEN @"screenName"
#define NAME_OF_EVENT @"eventName"
#define THE_VALUE @"eventValue"

@interface MCWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (nonatomic, strong) IBOutlet UIWebView *webView;

@end

@implementation MCWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/dinein/?locationId=%@&justEnteredPin=%@&flow=%@&appId=eb309ce405e45e7911f8ce6883b081b0",[[AFComunication sharedAFComunication]getURLPrefixString], [MyCheck sharedInstance].locationId, [AFComunication sharedAFComunication].justEnteredPin ? @"true" : @"false", [self paymentMethodForPaypalFlag:[[[MyCheck sharedInstance].userDefaults valueForKey:@"isPayPal"] boolValue]]];
    NSString *encodedString=[urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:encodedString]];
    NSString* agentStr = [[AFComunication sharedAFComunication]userAgentString];
    [request addValue:agentStr   forHTTPHeaderField:@"USER-AGENT"];
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:agentStr, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
    [self.webView loadRequest:request];
    [self.activityIndicatorView startAnimating];
    [self.webView setKeyboardDisplayRequiresUserAction:NO];
    [(UIScrollView *)[[self.webView subviews] lastObject] setScrollEnabled:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if(error.code!=ERROR_CANCEL){
    [self.activityIndicatorView removeFromSuperview];
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Failed loading web" message:@"Failed to load my check" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
    [alertView show];
    }
}

-(void)webViewDidFinishLoad:(UIWebView *)webView{
    [self.activityIndicatorView removeFromSuperview];
    if(self.tmpPin)
        [self.webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"setPwdAfterRegistration(%@)",self.tmpPin ]];

}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSURL*url=request.URL;
    NSString* urlString=url.absoluteString;
    NSLog(@"starting to load %@",urlString);
    NSRange textRange;
    textRange =[[urlString lowercaseString] rangeOfString:[@"app://close.takeaway" lowercaseString]];
    if(textRange.location != NSNotFound){
        [self dismissViewControllerAnimated:YES completion:nil];
        [self resetAppearance];

        return NO;
    }
    
    NSRange analyticsTextRange =[[urlString lowercaseString] rangeOfString:[@"MyCheck.Analitics?" lowercaseString]];
    if (textRange.location != NSNotFound) {
        NSString *paramsString = [urlString substringFromIndex:analyticsTextRange.length];
        NSArray *paramsArray = [paramsString componentsSeparatedByString:@"&"];
        NSMutableDictionary *paramsDictionary;
        for (NSString *param in paramsArray) {
            NSArray *chunks = [param componentsSeparatedByString:@"="];
            if (chunks.count) {
                [paramsDictionary setObject:paramsArray[1] forKey:paramsArray[2]];
            }
        }
        if ([MyCheck sharedInstance].appEventBlock) {
            [MyCheck sharedInstance].appEventBlock(paramsDictionary[NAME_OF_SCREEN], paramsDictionary[NAME_OF_EVENT], paramsDictionary[THE_VALUE]);
        }
        if ([MyCheck sharedInstance].appEventBlock) {
            [MyCheck sharedInstance].appEventBlock(paramsDictionary[NAME_OF_SCREEN], paramsDictionary[NAME_OF_EVENT], paramsDictionary[THE_VALUE]);
        }
    }

    return YES;
    
}
-(NSString *)paymentMethodForPaypalFlag:(BOOL)paypalFlag{
    return (paypalFlag) ? @"paypal" : @"mycheck";
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
