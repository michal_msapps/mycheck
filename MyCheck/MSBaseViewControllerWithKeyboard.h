//
//  BaseViewControllerWithKeyboard.h
//
//
//  Created by Michal Shatz on 12/11/13.
//  Copyright (c) 2013 Michal Shatz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MSBaseViewControllerWithKeyboard : UIViewController

@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) UITextField *activeField;

-(void)textFieldDidBeginEditing:(UITextField *)textField;

- (void)registerForKeyboardNotifications;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomOfScroll;

@end
