//
//  UILabel+Category.m
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "UILabel+Category.h"

@implementation UILabel (Category)

-(void)setFont:(UIFont *)fontForWords words:(NSArray *)words{
    NSMutableArray *rangeOfStrings = [[NSMutableArray alloc] init];
    for (NSString *word in words) {
        [rangeOfStrings addObjectsFromArray:[self rangesOfString:word inString:self.text]];
    }
    NSMutableAttributedString *attributedText = [[NSMutableAttributedString alloc] initWithString:self.text];
    for (NSValue *value in rangeOfStrings) {
        [attributedText setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:fontForWords.pointSize]} range:value.rangeValue];
    }
    self.attributedText = attributedText;
}

- (NSArray *)rangesOfString:(NSString *)searchString inString:(NSString *)str {
    NSMutableArray *results = [NSMutableArray array];
    NSRange searchRange = NSMakeRange(0, [str length]);
    NSRange range;
    while ((range = [str rangeOfString:searchString options:0 range:searchRange]).location != NSNotFound) {
        [results addObject:[NSValue valueWithRange:range]];
        searchRange = NSMakeRange(NSMaxRange(range), [str length] - NSMaxRange(range));
    }
    return results;
}

@end
