//
//  MCValidatedTextField.m
//  MyCheck
//
//  Created by Michal Shatz on 4/19/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCValidatedTextField.h"
#import "NSBundle+MCBundle.h"
#define NotValidatedImageName @"validation_no"
#define ValidatedImageName @"validation_yes"
#define ValidationImageSize 24.0f
#define ValidationImageInsets 10.0f
#define ValidationImageRelativeHeight 44.0f

@interface MCValidatedTextField ()

@property (nonatomic, strong) UIImageView *imgViewValidated;

@end

@implementation MCValidatedTextField

-(id)initialize{
    float aspectRatio = ValidationImageSize / ValidationImageRelativeHeight;
    self.imgViewValidated = [[UIImageView alloc] initWithFrame:CGRectMake(0,  0, self.frame.size.height * aspectRatio, self.frame.size.height * aspectRatio)];
    self.imgViewValidated.contentMode = UIViewContentModeRight;
    self.rightView = self.imgViewValidated;
    self.rightViewMode = UITextFieldViewModeAlways;
    self.imgViewValidated.hidden = YES;
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
       self = [self initialize];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
       self = [self initialize];
    }
    return self;
}

-(void)isValidated:(BOOL)validated{
    self.imgViewValidated.hidden = NO;
    if (validated) {
        self.imgViewValidated.image = [[NSBundle mycheckBundle] imageName:ValidatedImageName];
    } else {
        self.imgViewValidated.image = [[NSBundle mycheckBundle] imageName:NotValidatedImageName];
    }
}

- (CGRect) rightViewRectForBounds:(CGRect)bounds {
    CGRect textRect = [super rightViewRectForBounds:bounds];
    textRect.origin.x -= 10;
    return textRect;
}

-(void)showValidation:(BOOL)show{
    self.hidden = !show;
}

-(void)runValidationBlockOnString:(NSString *)string{
    [self isValidated:self.validationBlock(string)];
}



@end
