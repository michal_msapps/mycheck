//
//  Business.m
//  MyCheck
//
//  Created by Elad S on 4/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MCBusiness.h"
#import "MCOrder.h"
@implementation MCBusiness
@synthesize ID,Name,Category,Distance,hasWaiter,Address,City,quickPay,reward,showBenefitsBeforeTable;

-(id) initWithArr:(NSArray*)arr{
    self=[super init];
    if (self){
    self.ID=[arr objectAtIndex:0] ;
    self.Name=[arr objectAtIndex:1];
   self.Category= [arr objectAtIndex:2] ;
   self.Distance= [[arr objectAtIndex:3] intValue];
        self.hasWaiter=[[arr objectAtIndex:4]boolValue];
    }
    return self;
}

-(id) initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self){
        self.ID=[dic objectForKey:@"ID"];
        if(ID==nil || ![ID isKindOfClass:[NSString class]])
            return nil;
        self.Name=[dic objectForKey:@"Name"];
        if (Name==nil)
           self.Name=@""; 
        self.Category=[dic objectForKey:@"Category"];
        if(Category==nil)
            self.Category=@"";
        NSString* dis=[dic objectForKey:@"Distance"];
        if(dis!=nil && [dis isKindOfClass:[NSNumber class]]){
            self.Distance=[dis intValue];
        }else{
            self.Distance=0;
        }
        
        self.City=[dic objectForKey:@"City"];
        if (City==nil)
            self.City=@"";
        
        self.Address=[dic objectForKey:@"Address"];
        if (Address==nil)
            self.Address=@""; 
        self.hasWaiter=[[dic objectForKey:@"CallWaiter"] boolValue];
        
        NSNumber* num=[dic objectForKey:@"QuickPay"];
        if(num!=nil &&( [num isKindOfClass:[NSNumber class]]|| [num isKindOfClass:[NSString class]]))
        self.quickPay=[num boolValue];
       num=[dic objectForKey:@"HasBenefit"];
        if(num!=nil &&( [num isKindOfClass:[NSNumber class]]|| [num isKindOfClass:[NSString class]]))
            self.reward=[num boolValue];
        self.html=[dic objectForKey:@"Html"];
     //   self.html=[NSString stringWithFormat:@"https://test.mycheckapp.com/takeaway_jmobile/?bid=%@#home",self.ID];
        if (![self.html isKindOfClass:[NSString class]])
            self.html=nil;
        else
            self.html=[self.html stringByReplacingOccurrencesOfString:@"[BID]" withString:self.ID];

    
    num=[dic objectForKey:@"Latitude"];
        if( [num respondsToSelector:@selector(floatValue)])
        {
            self.lat=[num floatValue];
        }
        num=[dic objectForKey:@"Longitude"];
        if( [num respondsToSelector:@selector(floatValue)])
        {
            self.lon=[num floatValue];
        }
        self.Logo=[dic objectForKey:@"Logo"];
        if(![self.Logo isKindOfClass:[NSString class]])
            self.Logo=nil;
        self.locationId=[dic objectForKey:@"locationId"];
        if(self.locationId==nil)
            self.locationId=@"";
    }
    return self;
}
-(NSString*)getAdressString{
    return [Address stringByAppendingFormat:@", %@",City];
}
-(id)initWithOrder:(MCOrder* )order{
    self=[super init];
    if (self){
        self.ID=order.BID;
        self.Name=order.BizName;
        self.html=order.Html;
    }
    return self;
}
-(double)getDistaneFrom:(CLLocation*)other{
    if(other==nil)
        return 0;
    CLLocation* location=[[CLLocation alloc]initWithLatitude:self.lat longitude:self.lon];
    return [location distanceFromLocation:other];
}
-(NSString*)description{
    return self.Name;
}
@end
