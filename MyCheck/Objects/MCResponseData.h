//
//  ResponseData.h
//  MyCheck
//
//  Created by Elad S on 1/1/15.
//  Copyright (c) 2015 Elad S. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MCRequestData;
@interface MCResponseData : NSObject
@property(nonatomic,strong)MCRequestData * request;
@property(nonatomic,strong)NSDictionary* responseJSON;
@property(nonatomic,strong)NSError * error;
-(id) initWithRequest:(MCRequestData*)req json:(NSDictionary*)res error:(NSError*)err;
@end
