//
//  ResponseData.m
//  MyCheck
//
//  Created by Elad S on 1/1/15.
//  Copyright (c) 2015 Elad S. All rights reserved.
//

#import "MCResponseData.h"
#import "MCRequestData.h"
@implementation MCResponseData
-(id) initWithRequest:(MCRequestData*)req json:(NSDictionary*)res error:(NSError*)err{
    self = [super init];
    if(self){
        self.request = req;
        self. responseJSON = res;
        self. error = err;
    }
    return self;
}

@end
