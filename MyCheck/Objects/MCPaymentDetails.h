//
//  paymentDetails.h
//  MyCheck
//
//  Created by Elad S on 2/10/15.
//  Copyright (c) 2015 Elad S. All rights reserved.
//

#import <Foundation/Foundation.h>
@class MCOrder,MCPaymentType;
@interface MCPaymentDetails : NSObject
@property(strong,nonatomic) MCOrder *order;
@property(readwrite,nonatomic) float paymentAmount; // total amount to pay excluding the service
@property(readwrite,nonatomic) float serviceAmount;
@property(strong,nonatomic) MCPaymentType* paymentMethod;
@property(readwrite,nonatomic)BOOL sendInvoice;
@property(strong,nonatomic) NSMutableArray* items;// items to be payed for
@property(strong,nonatomic) NSString* applePayToken;
@property(strong,nonatomic) NSString* applePayCardType;
-(id)initWithOrder:(MCOrder*)order amount:(float)amount service:(float)service paymentMethod:(MCPaymentType*)method items:(NSMutableArray*)items sendInvoice:(BOOL)invoice;
-(id)initApplePayPaymentWithOrder:(MCOrder*)order amount:(float)amount service:(float)service applePayToken:(NSString*)token cardType:(NSString*)cardType items:(NSMutableArray*)items sendInvoice:(BOOL)invoice;
@end
