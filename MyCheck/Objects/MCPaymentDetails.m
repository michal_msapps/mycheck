//
//  paymentDetails.m
//  MyCheck
//
//  Created by Elad S on 2/10/15.
//  Copyright (c) 2015 Elad S. All rights reserved.
//

#import "MCPaymentDetails.h"
#import "MCPaymentType.h"
#import "MCOrder.h"
#import "MCItemBought.h"
@implementation MCPaymentDetails


/*
 helper constructor for the public constructors
 */
-(id)initWithOrder:(MCOrder*)order amount:(float)amount service:(float)service  items:(NSMutableArray*)items sendInvoice:(BOOL)invoice{
    self= [super init];
    if(self){
        self.order = order;
        self.paymentAmount = amount;
        self.serviceAmount = service;
        self.items = items;
        self.sendInvoice = invoice;
    }
    return self;
}
-(id)initWithOrder:(MCOrder*)order amount:(float)amount service:(float)service paymentMethod:(MCPaymentType*)method items:(NSMutableArray*)items sendInvoice:(BOOL)invoice{
    self = [self initWithOrder:order amount:amount service:service items:items sendInvoice:service];
    if(self ){
        self.paymentMethod=method;
    }
    return self;
}
-(id)initApplePayPaymentWithOrder:(MCOrder*)order amount:(float)amount service:(float)service applePayToken:(NSString*)token cardType:(NSString*)cardType items:(NSMutableArray*)items sendInvoice:(BOOL)invoice{
    self = [self initWithOrder:order amount:amount service:service items:items sendInvoice:service];
    if(self ){
        self.applePayCardType=cardType;
        self.applePayToken=token;
    }
    return self;
}
@end
