//
//  RequestData.h
//  MyCheck
//
//  Created by Elad S on 12/31/14.
//  Copyright (c) 2014 Elad S. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFComunication.h"
@interface MCRequestData : NSObject
@property(nonatomic,strong)NSDictionary* params;
@property(nonatomic,readwrite)APICall call;
@property(nonatomic,readwrite)NSString* method;
@property(nonatomic,readwrite)BOOL hasLoader;
-(id)initWithCall:(APICall)call params:(NSDictionary*)call method:(NSString*)method loader:(BOOL)loader;

@end
