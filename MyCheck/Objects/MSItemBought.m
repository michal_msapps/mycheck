//
//  ItemBought.m
//  MyCheck
//
//  Created by Elad S on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MCItemBought.h"


@implementation ItemBought
@synthesize  Name,ID,Cost,Quantity,Remarks,Tosafot,dictionary;

-initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self){
        self.ID=[dic objectForKey:@"ID"];
        if(ID==nil)
            self.ID=@"0";
        self.Name=[dic objectForKey:@"Name"];
        if(Name==nil)
            self.Name=@"";
        self.Cost= [[dic objectForKey:@"Price"]doubleValue];
        self.Quantity= [[dic objectForKey:@"Quantity"]floatValue];
        self.Remarks=[dic objectForKey:@"Remarks"];
        if(Remarks==nil)
            self.Remarks=@"";
        self.Tosafot=[dic objectForKey:@"Toppings"];
        if(Tosafot==nil)
            self.Tosafot=@"";
        self.dictionary=dic;
        self.wasBought= [[dic objectForKey:@"Paid"]boolValue];
        
    }
    return self;
    
}

- (id)copyWithZone:(NSZone *)zone
{
	ItemBought *myClassInstanceCopy = [[ItemBought allocWithZone: zone] init];
    
	myClassInstanceCopy.Name=[Name copy];
    myClassInstanceCopy.ID=[ID copy];
    myClassInstanceCopy.Cost=Cost;
    myClassInstanceCopy.Quantity=Quantity;
    myClassInstanceCopy.Remarks=[Remarks copy];
    myClassInstanceCopy.Tosafot=[Tosafot copy];
    myClassInstanceCopy.dictionary=[dictionary copy];
	return myClassInstanceCopy;
}
@end