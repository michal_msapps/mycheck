//
//  giftCard.m
//  
//
//  Created by Elad S on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MCPaymentType.h"

@implementation MCPaymentType
@synthesize ID,Type,Limit,Extra,Identifier;
-(id) initWithDic:(NSDictionary*)dic {
    
    self=[super init];
    if (self){
        self.ID=[dic objectForKey:@"ID"];
        if(ID==nil)
            self.ID=@"";
        self.Type=[dic objectForKey:@"Type"];
        if(Type==nil)
            self.Type=@"";
        
        self.Extra=[dic objectForKey:@"Extra"];
        if(Extra==nil)
            self.Extra=@"";
        self.Identifier=[dic objectForKey:@"Identifier"];
        if(Identifier==nil)
            self.Identifier=@"";
        NSRange isRange = [Type rangeOfString:@"Credit" options:NSCaseInsensitiveSearch];
        if(isRange.location == 0 && [NSLocalizedString(@"LOCALE", nil)isEqualToString:@"PT"]) {
            self.token=[dic objectForKey:@"token"];
            self.authorizerId=[dic objectForKey:@"authorizerId"];
        }
        
    }
  

    NSString*str=[dic objectForKey:@"Limit"];
    if(str!=nil && ([str isKindOfClass:[NSNumber class]] ||[str isKindOfClass:[NSString class]])){
        self.Limit=[str floatValue];
    }else{
        self.Limit=0;
    }
    _isPaypal = [[dic objectForKey:@"Type"] isEqualToString:@"PayPal"];
    return self;
}

-(id)initApplePayMethod{
    
    self=[super init];
    if (self){
        self.ID=APPLE_PAY_ID;
        self.Type=@"Applepay";
        self.Identifier=@"Apple Pay";

        self.Extra=@"Apple Pay";

    }
    return self;
}
-(NSString*)description{
    if([ self.Type rangeOfString:@"Applepay"].location==NSOrderedSame)
        return Extra;
    return Type;
}
@end
