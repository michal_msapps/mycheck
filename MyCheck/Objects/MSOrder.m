//
//  Order.m
//  MyCheck
//
//  Created by Elad S on 4/6/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "MCOrder.h"
#import "MCItemBought.h"
#define QUICKPAY_ONLY  false
@implementation MCOrder
@synthesize BizName,BID;
@synthesize OrderID;
@synthesize OrderTime,tipPercent;
@synthesize Date;
@synthesize TaxPercent;
@synthesize TaxAmount;
@synthesize SubTotal;
@synthesize Items;
@synthesize dictionary;
@synthesize Paid;
@synthesize Price;
@synthesize Service,paidTip,MaxTip;
@synthesize Balance,tableCode,noTip,User,quickPay,status,showFeedback;
- (id)copyWithZone:(NSZone *)zone
{
	MCOrder *myClassInstanceCopy = [[MCOrder allocWithZone: zone] init];
    
	myClassInstanceCopy.BizName=[BizName copy];
    myClassInstanceCopy.OrderID=[OrderID copy];
    myClassInstanceCopy.OrderTime=[OrderTime copy];
    myClassInstanceCopy.Date=[Date copy];
    //    myClassInstanceCopy.TaxPercent=[TaxPercent copy];
    //    myClassInstanceCopy.TaxAmount=[TaxAmount copy];
    myClassInstanceCopy.SubTotal=[SubTotal copy];
    myClassInstanceCopy.Items=[Items copy];
    myClassInstanceCopy.dictionary=[dictionary copy];
    myClassInstanceCopy.Paid=[Paid copy];
    myClassInstanceCopy.Price=[Price copy];
    myClassInstanceCopy.Service=[Service copy];
    myClassInstanceCopy.Balance=[Balance copy];
    myClassInstanceCopy.tableCode=[tableCode copy];
    myClassInstanceCopy.User=[User copy];
    myClassInstanceCopy.noTip=noTip;
    myClassInstanceCopy.TaxPercent=TaxPercent ;
    myClassInstanceCopy.TaxAmount=TaxAmount ;
    myClassInstanceCopy.tipPercent=tipPercent;
    myClassInstanceCopy.quickPay=quickPay;
    myClassInstanceCopy.iPaid=[_iPaid copy];


	return myClassInstanceCopy;
}

-(id)initWithDic:(NSDictionary*)dic{
    self=[super init];
    if (self){
       
            
        NSString* feedbackTmp=[dic objectForKey:@"Feedback"];
        if([feedbackTmp respondsToSelector:@selector(boolValue)]){
            self.showFeedback=[feedbackTmp boolValue];
        }
        self.BizName=[dic objectForKey:@"BizName"];
        if (![BizName isKindOfClass:NSString.class]) {
            BizName=@"null";
        }
        self.OrderTime=[dic objectForKey:@"Time"];
        if (![OrderTime isKindOfClass:NSString.class]) {
            OrderTime=@"null";
        }
        self.Date=[dic objectForKey:@"Date"];
        if (![Date isKindOfClass:NSString.class]) {
            Date=@"null";
        }
        self.tableCode=[dic objectForKey:@"ClientCode"];
        if (![tableCode isKindOfClass:NSString.class]) {
            tableCode=@"null";
        }
        self.BID=[dic objectForKey:@"BID"];
        if (![BID isKindOfClass:NSString.class]) {
            BID=@"";
        }
        self.locationId=[dic objectForKey:@"locationId"];
        if (![self.locationId isKindOfClass:NSString.class]) {
            self.locationId=@"";
        }
        self.Html=[dic objectForKey:@"Html"];
        if(![self.Html isKindOfClass:[NSString class]] || self.Html.length<5)
            self.Html=nil;
        else
         self.Html =   [self.Html stringByReplacingOccurrencesOfString:@"[BID]" withString:self.BID];
        
        //if([OrderID isKindOfClass:NSNumber.class])
        
        OrderID=[[NSString alloc] initWithFormat:@"%d",[[dic objectForKey:@"OrderID"] intValue]];
        // else
        //   OrderID=@"";
        
        if (![OrderID isKindOfClass:NSString.class]) {
            OrderID=@"null";
        }
        NSNumber* hasTip=[dic objectForKey:@"AllowTip"];
        if([hasTip isKindOfClass:[NSNumber class] ]){
            noTip=![hasTip boolValue];
        }
        NSArray* tmp=[dic objectForKey:@"Items"];
        if([tmp isMemberOfClass:[NSNull class]]){
            self.Items=[NSArray arrayWithObjects: nil];
        }
        else{
            NSMutableArray* arr=[[NSMutableArray alloc] initWithCapacity:[tmp count]];
            for(NSDictionary* d in tmp){
                ItemBought* item=[[ItemBought alloc]initWithDic:d];
                if([item.Name isEqualToString:@"Tax"])
                    continue;
                [arr addObject:item];
                // sum+=item.Cost;
            }
            self.Items=[NSArray arrayWithArray:arr];
        }
        self.dictionary=dic;
        
        self.SubTotal=[NSString stringWithFormat:@"%@",[dic objectForKey:@"Subtotal"]];
        
    }
//    NSArray* total=[dic objectForKey:@"Total"];
    //if([total count]>0){
    NSDictionary* totalD=[dic objectForKey:@"Total"];
    Price=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"Price"] floatValue]];
    Paid=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"Paid"] floatValue]];
    self.iPaid=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"iPaid"] floatValue]];

    Service=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"Service"] floatValue]];
    //Balance=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"Balance"] floatValue]];
    Balance=[[NSString alloc] initWithFormat:@"%0.2f",[[dic objectForKey:@"Subtotal"] floatValue]];
    
    User=[[NSString alloc] initWithFormat:@"%0.2f",[[totalD objectForKey:@"User"] floatValue]];
    
    //  }
    
    NSString*taxStr=[dic objectForKey:@"Tax"];
    if (![taxStr isKindOfClass:NSString.class]) {
        TaxAmount=0;
    }else{
        TaxAmount=[taxStr floatValue];
    }
    if([dic objectForKey:@"Percent"] && ![[dic objectForKey:@"Percent"] isKindOfClass:[NSNull class]]){
        TaxPercent=[[dic objectForKey:@"Percent"] floatValue]/100.0;
    }else
        TaxPercent=0;
    
    if([dic objectForKey:@"PercentageTip"] && ![[dic objectForKey:@"PercentageTip"] isKindOfClass:[NSNull class]]){
        tipPercent=[[dic objectForKey:@"PercentageTip"] floatValue]/100.0;
    }else{
        tipPercent=0.0;
    }
    if([NSLocalizedString(@"LOCALE", nil)isEqualToString:@"PT"]){
        if([totalD objectForKey:@"servicePerc"] && ![[totalD objectForKey:@"servicePerc"] isKindOfClass:[NSNull class]]){
            self.servicePercent=[[totalD objectForKey:@"servicePerc"] floatValue]/100.0;
        }else{
            tipPercent=0.0;
        }

    }    if([totalD objectForKey:@"iPaidTip"] && ![[totalD objectForKey:@"iPaidTip"] isKindOfClass:[NSNull class]]){
        paidTip=[[totalD objectForKey:@"iPaidTip"] floatValue];
    }else{
        paidTip=-1;
    }

            //TaxPercent=(priceF-TaxAmount<=0)?0:TaxAmount/(priceF-TaxAmount);
//    if(![NSLocalizedString(@"CURRENCY", nil)isEqualToString:@"$"]){
//        TaxAmount=TaxPercent=0.0f;
//    }
    self.status=[dic objectForKey:@"Status"];
    if (![status isKindOfClass:NSString.class]) {
        status=@"null";
    }

    
    NSNumber* quick=[dic objectForKey:@"QuickPay"];
    if([quick isKindOfClass:[NSNumber class] ]){
        quickPay=[quick boolValue] || QUICKPAY_ONLY;
    }
    
    
    NSNumber* mt=[dic objectForKey:@"MaxTip"];
    if([mt isKindOfClass:[NSNumber class]] || [mt isKindOfClass:[NSString class]] ){
        MaxTip=[mt floatValue]/100;
    }
    

            return self;
        }

-(NSString*)description{
return [NSString stringWithFormat:@"order ID:%@ order subtotal:%@",OrderID,SubTotal];
}
        @end
