//
//  ItemBought.h
//  MyCheck
//
//  Created by Elad S on 4/3/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ItemBought : NSObject {
    
    NSString* Name;
    NSString* ID;
    double Cost;
    float Quantity;
    NSString* Remarks;
    NSString* Tosafot;
    NSDictionary* dictionary;
    
}
@property(readwrite)BOOL wasBought;
@property(readwrite)BOOL wasSelected;

@property(strong,nonatomic) NSString* Name;
@property(strong,nonatomic)NSString* ID;
@property(readwrite)double Cost;
@property(readwrite)float Quantity;
@property(strong,nonatomic)NSString* Remarks;
@property(strong,nonatomic)NSString* Tosafot;
@property(strong,nonatomic) NSDictionary* dictionary;
-initWithDic:(NSDictionary*)dic;
@end
