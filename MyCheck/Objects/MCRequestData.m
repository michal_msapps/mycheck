//
//  RequestData.m
//  MyCheck
//
//  Created by Elad S on 12/31/14.
//  Copyright (c) 2014 Elad S. All rights reserved.
//

#import "MCRequestData.h"

@implementation MCRequestData
-(id)initWithCall:(APICall)call params:(NSDictionary*)prams loader:(BOOL)loader{
    self = [super init];
    if(self){
        self.call=call;
        self.params=prams;
        self.method=@"POST";
        self.hasLoader=loader;

    }
    return self;
}
-(id)initWithCall:(APICall)call params:(NSDictionary*)params method:(NSString*)method loader:(BOOL)loader{
    self = [self initWithCall:call params:params loader:loader];
    if(self){
        self.method=method;
    }
    return self;
}

@end
