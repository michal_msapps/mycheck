//
//  giftCard.h
//  
//
//  Created by Elad S on 1/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#define APPLE_PAY_ID @"Apple pay id 344332"
@interface MCPaymentType : NSObject
{

    NSString*ID;
    NSString*Type;
    float Limit;
    NSString* Extra;
    NSString* Identifier;
}
 -(id) initWithDic:(NSDictionary*)dic;
@property(strong,nonatomic)NSString*ID;
@property(strong,nonatomic)NSString*Type;
@property(readwrite)float Limit;
@property(strong,nonatomic) NSString* Extra;
@property(strong,nonatomic) NSString* Identifier;
@property(strong,nonatomic)NSString* token;
@property(strong,nonatomic)NSString*authorizerId;
@property(readonly,nonatomic)BOOL isPaypal;
-(id)initApplePayMethod;
@end
