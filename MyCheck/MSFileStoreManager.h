//
//  FileStoreManager.h
//  Drippler
//
//  Created by Drippler on 8/24/14.
//  Copyright (c) 2014 Drippler. All rights reserved.
//

/**
 * FileStoreManager manage the creation and pulling of files from disk.
 */

#import <Foundation/Foundation.h>

@interface MSFileStoreManager : NSObject

/**
 * Return the error caused by the creation of a new file. 
 * If error is nil creation succeded.
 * @param name The file name.
 * @param type The file type and derived extension.
 * @return the error caused by creation of a file with given name and type in disk.
 */
+(NSError *)createFileIfDoesNotExistWithName:(NSString *)name andExtension:(NSString *)type;

/**
 * Return path for file with name and type.
 * @param name The file name.
 * @param type The file type and derived extension.
 * @return full file path to the file described by params.
 */
+(NSString *)pathForFileName:(NSString *)name withType:(NSString *)type;

@end
