//
//  NSString+Category.h
//  MyCheck
//
//  Created by Michal Shatz on 4/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Category)

+(NSString *)stringWithBool:(BOOL)booleanValue;
-(BOOL)validateEmail;


@end
