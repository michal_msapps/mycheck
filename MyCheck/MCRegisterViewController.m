//
//  RegisterViewController.m
//  
//
//  Created by Michal Shatz on 3/18/15.
//
//

#import "MCRegisterViewController.h"
#import "AFComunication.h"
#import "MCCreditCardViewController.h"
#import "MyCheck.h"
#import "NSBundle+MCBundle.h"
#import <QuartzCore/QuartzCore.h>
#import "UITextField+Category.h"
#import "MCValidatedTextField.h"
#import <MyCheck/MyCheck.h>
#import "NSString+Category.h"
#import "MCTermsAndConditionsViewController.h"

@interface MCRegisterViewController () <UITextFieldDelegate, UIPickerViewDelegate, UIAlertViewDelegate>

{
    BOOL keyboardShown;
    CGPoint originalOffset;//the original offset of the keyboard


}

@property (weak, nonatomic) IBOutlet MCValidatedTextField *txtName;
@property (weak, nonatomic) IBOutlet MCValidatedTextField *txtLastName;
@property (weak, nonatomic) IBOutlet MCValidatedTextField *txtEmail;
@property (weak, nonatomic) IBOutlet MCValidatedTextField *txtPassword;
@property (weak, nonatomic) IBOutlet MCValidatedTextField *txtReenterPassword;
@property (weak, nonatomic) IBOutlet UIButton *btnSignUp;
@end

@implementation MCRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topBar.lblTitle.text = @"Register";
    self.txtEmail.validationBlock = ^BOOL(NSString *text){
        BOOL returnValue = YES;
        NSArray* mailArr=[self.txtEmail.text componentsSeparatedByString:@"@"];
        if([mailArr count]!=2 || [((NSString*)[mailArr objectAtIndex:1] )rangeOfString:@"."].location==NSNotFound){
            returnValue = NO;
        }
        return text.length > 0 && returnValue;
    };
    self.txtLastName.validationBlock = ^BOOL(NSString *text){
        return text.length > 0;
    };
    self.txtName.validationBlock = ^BOOL(NSString *text){
        return text.length > 0;
    };
    self.txtPassword.validationBlock = ^BOOL(NSString *text){
        return text.length == 4;
    };
    self.txtReenterPassword.validationBlock = ^BOOL(NSString *text){
        return [text isEqualToString:self.txtPassword.text];
    };
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.btnSignUp.enabled = YES;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)register:(id)sender {
    self.btnSignUp.enabled = NO;
    BOOL hasError=NO;
    NSMutableString * errorMsg=[NSMutableString string];
    [errorMsg appendString:NSLocalizedString(@"Please fill in:\n",nil)];
    if([self.txtName.text length]==0){
        hasError=YES;
        [errorMsg appendString:NSLocalizedString(@"First Name\n",nil)];
    }
    if([self.txtLastName.text length]==0){
        hasError=YES;
        [errorMsg appendString:NSLocalizedString(@"Last name\n",nil)];
    }
    if([self.txtPassword.text length]!=4 ){
        hasError=YES;
        [errorMsg appendString:NSLocalizedString(@"A 4-Digit PIN\n",nil)];
    }else if(![self.txtPassword.text isEqualToString:self.txtReenterPassword.text]){
        hasError=YES;
        errorMsg=[[errorMsg stringByReplacingOccurrencesOfString:NSLocalizedString(@"Please fill in:",nil) withString:@""] mutableCopy];
        [errorMsg appendString:NSLocalizedString(@"Reentry of PIN doesn't match\n",nil)];
    }
    
    if([self.txtEmail.text length]==0 ){
        hasError=YES;
        [errorMsg appendString:NSLocalizedString(@"Email\n",nil)];
    }
    else{
        NSArray* mailArr=[self.txtEmail.text componentsSeparatedByString:@"@"];
        if([mailArr count]!=2 || (([((NSString*)[mailArr objectAtIndex:1] )rangeOfString:@"."].location==NSNotFound) && ([self.txtEmail.text validateEmail]))){
            hasError=YES;
            [errorMsg appendString:NSLocalizedString(@"Enter proper Email\n",nil)];
        }
    }
    if(hasError){
        UIAlertView* alert=[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR",nil) message:errorMsg delegate:self cancelButtonTitle:NSLocalizedString(@"OK",nil) otherButtonTitles: nil];
        [alert show];
    }else{
        [[AFComunication sharedAFComunication] registerWithName:self.txtName.text surname:self.txtLastName.text email:self.txtEmail.text pin:self.txtPassword.text phone:nil dob:nil pushToken:nil FBToken:nil success:^{
            [AFComunication sharedAFComunication].justEnteredPin = YES;
            [[AFComunication sharedAFComunication] loginWithEmail:self.txtEmail.text pin:self.txtPassword.text pushToken:nil FBToken:nil success:^(BOOL hasPMethod){
                [AFComunication sharedAFComunication].hasPaymentMethod = hasPMethod;
                [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:hasPMethod] forKey:@"hasPaymentMethod"];
                MCCreditCardViewController *creditCardViewController = [[MCCreditCardViewController alloc] initWithNibName:@"MCCreditCardViewController" bundle:[NSBundle mycheckBundle]];
                [[MyCheck sharedInstance].userDefaults setValue:self.txtEmail.text forKey:@"userEmail"];
                 creditCardViewController.tmpPin=self.txtPassword.text;
                [self.navigationController pushViewController:creditCardViewController animated:YES];
            } failure:^(MCResponseData *response, NSError *error) {
                UIAlertView *alertFailedToRegister = [[UIAlertView alloc] initWithTitle:@"Registration Error" message:error.localizedDescription delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alertFailedToRegister show];
            }];
        } failure:^(MCResponseData *response, NSError *error) {
            UIAlertView *alertFailedToRegister = [[UIAlertView alloc] initWithTitle:@"Registration Error" message:error.localizedDescription delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            alertFailedToRegister.delegate = self;
            [alertFailedToRegister show];
            self.btnSignUp.enabled = YES;
        }];
    }
}

-(IBAction)termsAndConditions:(id)sender{
    MCTermsAndConditionsViewController *termsAndConditionsViewController = [[MCTermsAndConditionsViewController alloc] initWithNibName:@"MCTermsAndConditionsView" bundle:[NSBundle mycheckBundle]];
    [self.navigationController pushViewController:termsAndConditionsViewController animated:YES];
}

#pragma mark UITextFieldDelegate

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    BOOL returnValue = YES;
    if ((textField == self.txtPassword || textField == self.txtReenterPassword) && range.location + string.length > 4) {
        returnValue = NO;
    } else {
        if ([textField isKindOfClass:[MCValidatedTextField class]]) {
            MCValidatedTextField *mcValidatedTextField = (MCValidatedTextField *)textField;
            NSString * newStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
            [mcValidatedTextField runValidationBlockOnString:newStr];
        }
    }
    return returnValue;
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.txtReenterPassword) {
        [textField addNumberToolBarWithTarget:self andAction:@selector(textFieldShouldReturn:)];
    } else if (textField == self.txtPassword) {
        [textField addNumberToolBarWithTarget:self titles:@[@"Cancel", @"Next"] andActions:@[NSStringFromSelector(@selector(textFieldShouldReturn:)), NSStringFromSelector(@selector(textFieldShouldReturn:))]];
    }
    return YES;
}

#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    self.btnSignUp.enabled = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
