//
//  UITextField+Category.m
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "UITextField+Category.h"

@implementation UITextField (Category)



-(void)addNumberToolBar{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 5/48)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(dismissKeyboard)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(dismissKeyboard)],
                           nil];
    [numberToolbar sizeToFit];
    self.inputAccessoryView = numberToolbar;
}

-(void)addNumberToolBarWithTarget:(id)target andAction:(SEL)action{
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 5/48)];
    numberToolbar.barStyle = UIBarStyleDefault;
    numberToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:target action:action],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:target action:action],
                           nil];
    [numberToolbar sizeToFit];
    self.inputAccessoryView = numberToolbar;
}

-(void)addNumberToolBarWithTarget:(id)target titles:(NSArray *)titles andActions:(NSArray *)actions{
    if (titles.count != actions.count) {
        [NSException raise:@"You should have as much actions as you have title" format:nil];
    }
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height * 5/48)];
    numberToolbar.barStyle = UIBarStyleDefault;
    NSMutableArray *actionButtonsArray = [[NSMutableArray alloc] initWithCapacity:titles.count];
    for (NSUInteger actionIndex = 0; actionIndex < titles.count; actionIndex++) {
        UIBarButtonItem *actionItem = [[UIBarButtonItem alloc] initWithTitle:titles[actionIndex] style:UIBarButtonItemStylePlain target:target action:NSSelectorFromString(actions[actionIndex])];
        [actionButtonsArray addObject:actionItem];
    }
    [actionButtonsArray insertObject:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil] atIndex:floor(titles.count / 2)];
    numberToolbar.items = actionButtonsArray;
    [numberToolbar sizeToFit];
    self.inputAccessoryView = numberToolbar;
}

-(void)dismissKeyboard{
    [self resignFirstResponder];
}

@end
