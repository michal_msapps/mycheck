//
//  MCMyCheckLoginViewController.m
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCMyCheckLoginViewController.h"
#import "AFComunication.h"
#import "MCWebViewController.h"
#import "MCRegisterViewController.h"
#import "MyCheck.h"
#import "NSBundle+MCBundle.h"
#import "UITextField+Category.h"
#import "MCCreditCardViewController.h"
#define NO_PAYMENT_METHOD_ALERT 193985
@interface MCMyCheckLoginViewController () <UIAlertViewDelegate, UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *txtEmailAddress;
@property (nonatomic, strong) IBOutlet UITextField *txtPin;
@property (nonatomic, strong) IBOutlet UIButton *btnLogin;

@end

@implementation MCMyCheckLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.topBar.lblTitle.text = @"MyCheck";
    self.topBar.delegate = self;
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.btnLogin.enabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)login:(id)sender{
    self.btnLogin.enabled = NO;
    if (self.txtPin.text && self.txtEmailAddress.text) {
        [[AFComunication sharedAFComunication] loginWithEmail:self.txtEmailAddress.text pin:self.txtPin.text pushToken:nil FBToken:nil success:^(BOOL hasPMethod){
            [AFComunication sharedAFComunication].hasPaymentMethod = hasPMethod;
            [[MyCheck sharedInstance].userDefaults setValue:[NSNumber numberWithBool:hasPMethod] forKey:@"hasPaymentMethod"];
            [AFComunication sharedAFComunication].justEnteredPin = YES;
            [[MyCheck sharedInstance].userDefaults setValue:self.txtEmailAddress.text forKey:@"userEmail"];
            if(hasPMethod){
            MCWebViewController *mcWebViewController = [[MCWebViewController alloc] initWithNibName:@"MCWebViewController" bundle:[NSBundle mycheckBundle]];
           
            mcWebViewController.tmpPin=self.txtPin.text;
            [self.navigationController pushViewController:mcWebViewController animated:YES];
            }else{//send to add credit card screen
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please add a payment method" message:@"" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
                alert.tag = NO_PAYMENT_METHOD_ALERT;
            }
        } failure:^(MCResponseData *response, NSError *error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:error.localizedDescription delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alertView show];
            self.btnLogin.enabled = YES;
        }];
    } else {
        UIAlertView *noInputAlertView = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"User name and password are required" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [noInputAlertView show];
        self.btnLogin.enabled = YES;
    }
}


- (IBAction)register:(id)sender {
    MCRegisterViewController *registerViewController = [[MCRegisterViewController alloc] initWithNibName:@"MCRegisterView" bundle:[NSBundle mycheckBundle]];
    [self.navigationController pushViewController:registerViewController animated:YES];
}

-(IBAction)forgotPassword:(id)sender{
    NSString *userEmail = [[MyCheck sharedInstance].userDefaults valueForKey:@"userEmail"];
    if (!userEmail) {
        UIAlertView *alertForgotPassword = [[UIAlertView alloc] initWithTitle:nil message:@"Enter your email address and we'll help you reset your password." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alertForgotPassword.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alertForgotPassword textFieldAtIndex:0].text = self.txtEmailAddress.text;
        [alertForgotPassword textFieldAtIndex:0].keyboardType=UIKeyboardTypeEmailAddress;
        [alertForgotPassword show];
    } else {
        [self forgotPasswordWithEmail:userEmail];
    }
   
}

-(void)forgotPasswordWithEmail:(NSString *)email{
    [[AFComunication sharedAFComunication] forgotPinForEmail:email Success:^(NSString *successMsg) {
        UIAlertView *succesAlert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Check your inbox for instruction on resetting your password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [succesAlert show];
    } failure:^(MCResponseData *response, NSError *error) {
        UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:@"Failure" message:@"This e-mail address is not registered. Please try another e-mail." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [failAlert show];
    }];
}

#pragma mark UIAlertViewDelegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView.tag == NO_PAYMENT_METHOD_ALERT){
        MCCreditCardViewController *creditCardViewController = [[MCCreditCardViewController alloc] initWithNibName:@"MCCreditCardViewController" bundle:[NSBundle mycheckBundle]];
        creditCardViewController.tmpPin = self.txtPin.text;
        [self.navigationController pushViewController:creditCardViewController animated:YES];

    }else{
    UITextField *emailTextField = [alertView textFieldAtIndex:0];
    NSMutableString *errorMsg;
    BOOL hasError = NO;
    if([emailTextField.text length]==0 ){
        hasError=YES;
        [errorMsg appendString:NSLocalizedString(@"Email\n",nil)];
    }
    else{
        NSArray* mailArr=[emailTextField.text componentsSeparatedByString:@"@"];
        if([mailArr count]!=2 || [((NSString*)[mailArr objectAtIndex:1] )rangeOfString:@"."].location==NSNotFound){
            hasError=YES;
            [errorMsg appendString:NSLocalizedString(@"Enter proper Email\n",nil)];
        }
    }
    if (!hasError) {
        [self forgotPasswordWithEmail:emailTextField.text];
    } else {
        UIAlertView *emailInputError = [[UIAlertView alloc] initWithTitle:@"Bad Email Address" message:errorMsg delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [emailInputError show];
    }
    }
}

#pragma mark UITextField

// allow up to 4 characters in password
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (range.location + string.length > 4 && textField.tag == 2) {
        return NO;
    } else {
        return YES;
    }
}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    if (textField == self.txtPin) {
        [textField addNumberToolBar];
    }
    return YES;
}


#pragma UIAlertViewDelegate


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
