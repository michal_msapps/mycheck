//
//  MCPaymentMethod.m
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCPaymentMethod.h"
#import <QuartzCore/QuartzCore.h>
#import "UIColor+MCColor.h"

@implementation MCPaymentMethod

-(void)drawRect:(CGRect)rect{
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = [UIColor myCheckMagento].CGColor;
    self.layer.cornerRadius = 4.0f;
}

@end
