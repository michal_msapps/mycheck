//
//  QuickPayModel.h
//  MyCheck
//
//  Created by Elad S on 2/21/13.
//
//

#import <Foundation/Foundation.h>

@class MCOrder,AppDelegate;
/*
 the varies states of the order
 */
typedef enum PaymentStatus{DONE,FAILED,PENDING,UNDIFINED,NO_OPEN_ORDER,OPEN,CANCELED}PaymentStatus;

@protocol PaymentStatusDelegate
/*called only if the status of the order changes
 @param status 
        the new status of the order
 @order 
        the order object or nil if the order was, for example, closed
 */
-(void)PaymentStatusChanged:(PaymentStatus)status order:(MCOrder*)order;
/*
 called every time the order is updated from the server (even if it remains the same
 @param order 
        the updated order
 */
-(void)newOrder:(MCOrder*)order;

/*
 
 */

@end

@interface MCQuickPayModel : NSObject {
	NSString *currentStatus;
	id<PaymentStatusDelegate> delegate;

}

-(id)initWithDelegate:(id<PaymentStatusDelegate>)del pollInterval:(float)interval;
-(void)startPolling;
-(void)stopPolling;
/*
 resets the polling proccess
 */
-(void)reset;
/*the table code.
 can be used if an order isn't open yet.
 */
@property(strong,nonatomic)NSString*code;
@property(strong,nonatomic)	NSString *currentStatus;
@property(strong,nonatomic)	id<PaymentStatusDelegate> delegate;

@end
