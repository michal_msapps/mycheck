//
//  MCValidatedTextField.h
//  MyCheck
//
//  Created by Michal Shatz on 4/19/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef BOOL (^MCValidationBlock)(NSString *);

@interface MCValidatedTextField : UITextField

-(void)isValidated:(BOOL)validated;
-(void)showValidation:(BOOL)show;

@property (copy, nonatomic) MCValidationBlock validationBlock;

-(void)runValidationBlockOnString:(NSString *)string;

@end
