

#import "AFHTTPSessionManager.h"
#define AF_DEBUG 1



#define NOT_LOGGED_IN_CODE 17

/*
 defines weather to use the production MyCheck server or the debug server
 */
@class MCRequestData,MCResponseData,CLLocation,MCOrder,MCPaymentDetails;

/*
 for internal use
 */
typedef enum {
    AF_NOTHING
    , AF_REGISTRATION
    , AF_LOGIN
    , AF_CREDIT
    , AF_NEARBY
    , AF_CLIENT_CODE
    , AF_VIEW_ORDER
    , AF_PAYMENT
    , AF_CREDIT_DETAILS
    , AF_LOGOUT
    , AF_CALL_WAITER
    , AF_SET_USER
    , AF_GET_USER_DETAILS
    , AF_PREVIOUS
    , AF_FORGOT
    , AF_CREDIT_CHECK
    , AF_CAN_LEAVE_CHECK
    , AF_NOT_LOGEDIN
    , AF_WAIT_FOR_IT_MSG
    , AF_PAYENT_STATUS
    , AF_WRONG_PW
    , AF_AD
    , AF_FACEBOOK
    , AF_AD_PIC
    , AF_STATISTICS
    , AF_PAYMENT_METHOD
    , AF_GIFT_CARD_LIST
    , AF_BUY_GIFT_CARD
    , AF_SEND_GIFT_CARD_TO_FRIEND
    , AF_CHECK_INVITATION_STATUS
    , AF_SEND_JOIN_INVITATION_TO_FRIEND
    , AF_GET_BENIFIT_LIST
    , AF_GET_NIKOOV
    , AF_GET_BENIFIT
    , AF_LIST_FOR_GIFT_CARD
    , AF_IS_BENEFIT_VALID
    , AF_GET_PLACE_NOTIFICATION
    , AF_GET_VARS_AND_STRINGS
    , AF_REGISTER_NOTIFICATION
    , AF_GET_OFFLINE_BIZLIST
    , AF_GET_OFFLINE_BENEFIT_LIST
    , AF_REGISTER_BENEFIT
    , AF_BENEFIT_COUNT
    , AF_ADD_USER_TO_TABLE
    , AF_PANGO_REG_FIRST
    , AF_PANGO_REG_SECOND
    , AF_PANGO_TO_MYCHECK
    , AF_CHANGE_PAYMENT_METHOD
    , AF_PROMO_CODE
    , AF_SEND_RECIPT_IN_EMAIL
    , AF_POST_PAYMENT
    , AF_GET_COUPON_DETAILS
    , AF_DELETE_PAYMENT_METHOD
    , AF_GET_PLACE_INFO
    , AF_GET_BENEFIT_HTML
    , AF_GET_INVITATION_MSG
    , AF_SEND_FEEDBACK
    , AF_GET_BAlANCE
    , AF_PAS_TO_PIN
    , AF_POPUP_BENEFIT
    , AF_GET_GAS_NUMBER
    , AF_GET_MEMBER_LIST
    , AF_UPDATE_PAYED_ITEMS
    , AF_BIZ_LIST_CITY
    , AF_GET_URLS
    , AF_VALIDATE_ZIP
    , AF_REWARD_LIST
    , AF_CANCEL_BENEFIT
    , AF_ADD_CREDIT_CARD
    , AF_AUTHENTICATE_PAYPAL
}APICall;

typedef enum {UNKNOWN_LOGIN_STATE,LOGGEDIN,LOGDOUT}LoginState;
@interface AFComunication : AFHTTPSessionManager{
}
/*
 in order to manualy set the server you want to use call this function to create the instance for the first time
 */
+(AFComunication*)sharedAFComunicationFirstTimeProduction:(BOOL)prod;
/*
 get the shared singlton.
 This is the only way you should create a new  instance.
 */
+ (AFComunication *)sharedAFComunication;

/*
 setup a the instance once.
 @param appt
 the appType for the application (if you dont know this ask someone in mycheck)
 */
-(void)configWithAppType:(NSString*)appT  deviceId:(NSString*)deveiceId;


//- (void)updateWeatherAtLocation:(CLLocation *)location forNumberOfDays:(NSUInteger)number;

/*
 a conviniance default error block that simply displays an alert with the error message
 */
@property (nonatomic, strong) void (^showFailAlert)(MCResponseData * response, NSError *error);


@property (nonatomic,strong) NSString* locale;
//convinance method
-(NSURLSessionDataTask*)sendRequest:(APICall)call withArgs:(NSDictionary*)args load:(BOOL)show
           success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
           failure:(void (^)(MCResponseData * response, NSError *error))failure;
-(NSURLSessionDataTask*) sendRequest:(MCRequestData*)req  success:(void (^)(NSURLSessionDataTask *task, MCResponseData * responseObject))success
            failure:(void (^)(MCResponseData * response, NSError *error))failure;

//send multiple request with no order
-(void)sendRequests:(NSMutableArray*)requests
            success:(void (^)( NSMutableArray* responses))success
            failure:(void (^)(MCResponseData * response, NSError *error))failure;


#pragma API call specific functions
/* register a new user 
 @param name 
        the users name
 @param sname 
        the users surname
 @param email 
        the users email
 @param pin
        the pin code the user selected (should be a 4 digit numeric number)
 @param phone
        the users phone number OPTIONAL
 @param dob 
        the users date of birth OPTIONAL
 @param token 
        the devices token used for push notifications or nil if you dont have one OPTIONAL
 @param fb
        the Facebook access token or nil if you havent used Facebook OPTIONAL
 @param sucess 
        the block that is called if and when the call succeeds OPTIONAL
 @param failure 
        the block that is called if and when the call fails OPTIONAL
 
 
 */
-(NSURLSessionDataTask*)registerWithName:(NSString*)name surname:(NSString*)sname email:(NSString*)email pin:(NSString*)pin phone:(NSString*)phone dob:(NSString*)dob pushToken:(NSString*)token FBToken:(NSString*)fb success:(void (^)())success
                            failure:(void (^)(MCResponseData * response, NSError *error))failure;


/* login a user

 @param email
 the users email
 @param pin
 the pin code the user selected (should be a 4 digit numeric number)
 @param token
 the devices token used for push notifications or nil if you dont have one OPTIONAL
 @param fb
 the Facebook access token or nil if you havent logged in with Facebook OPTIONAL
 @param sucess
 the block that is called if and when the call succeeds OPTIONAL
 @param failure
 the block that is called if and when the call fails OPTIONAL
 
 */
-(NSURLSessionDataTask*)loginWithEmail:(NSString*)email pin:(NSString*)pin pushToken:(NSString*)token FBToken:(NSString*)fb success:(void (^)(BOOL hasPaymentMethod))success
                                 failure:(void (^)(MCResponseData * response, NSError *error))failure;

/* logout a user
 
 */
-(NSURLSessionDataTask*)logoutSuccess:(void (^)())success
                               failure:(void (^)(MCResponseData * response, NSError *error))failure;


/* returns the status of a punch card
 
 @param benID
 the punch card id
 
 
 */
-(NSURLSessionDataTask*)getNikoovForBenefitId:(NSString*)benID
                     success:(void (^)( NSInteger count, NSInteger quantity))success
                     failure:(void (^)(MCResponseData * response, NSError *error))failure;
-(NSURLSessionDataTask*)getBenefitsHTMLForBenefitId:(NSString*)benID chainID:(NSString*)chainID
                     success:(void (^)( NSString* html))success
                     failure:(void (^)(MCResponseData * response, NSError *error))failure;


/*
 Returns urls 
 @param appType
 */
-(NSURLSessionDataTask *)getUrlsForAppType:(NSString *)appType success:(void (^)(MCResponseData * responseObject))success failure:(void (^)(MCResponseData * responseObject, NSError *error))failure;

/* Report to the server that the user forgot his/her Pin. the server will send an email with further instructions.
 
 @param email
        the email of the user that forgot his/her pin
 
 
 */
-(NSURLSessionDataTask*)forgotPinForEmail:(NSString*)email Success:(void (^)(NSString* successMsg))success
                                  failure:(void (^)(MCResponseData * response, NSError *error))failure;


/* get app strings
 */
-(NSURLSessionDataTask *)getAppStringsSuccess:(void (^)(MCResponseData * responseObject, NSString* successMsg))success
                                      failure:(void (^)(MCResponseData * responseObject, NSError *error))failure;

/* get a 4 digit code from the server that the user can show to the waitress in order to open a table.
 
 @param BID
        the business ID
 @param pin
        the pin code or nil if non is needed
 
 */
-(NSURLSessionDataTask*)getClientCodeForBusinessID:(NSString*)BID PIN:(NSString*)pin
                          success:(void (^)( NSString* code,NSString* text,bool payFull))success
                          failure:(void (^)(MCResponseData * response, NSError *error))failure;


/* get a list of businesses arranged according to distance from the specified location
 @param location
        the location you want to find businesses around
 @param recent
        true if you want to show businesses that the current user recently dined in.
 @param cat
        the category of the business or nil OPTIONAL

 
 */


-(NSURLSessionDataTask*)getNearbyBusinessWithLocation:(CLLocation*)location recent:(BOOL)recent Category:(NSString*)cat
                             success:(void (^)( NSMutableArray* businesses,NSString* md5))success
                             failure:(void (^)(MCResponseData * response, NSError *error))failure;


/* get the order details for a specific order ID
if orderID is nil the function will return the order that is currently open or fail.
 @param orderID 
        the current order ID or nil OPTIONAL
 @param code 
        if you dont have an orderId yet you can use the 4 digit code OPTIONAL
 */
-(NSURLSessionDataTask*)getOrderDetailsForOrderID:(NSString*)orderID tableCode:(NSString*)code polling:(BOOL)polling
                                              success:(void (^)( MCOrder* order))success
                                              failure:(void (^)(MCResponseData * task, NSError *error))failure;


/* checks wether or not a table is already open for the logged in user. if it does a locationId of the business the table was opened in is returned.
 @param open returns weather or not a table is open for the logged in user
 @param locationId the locationId the table is open at or nil if open == NO

 */
-(NSURLSessionDataTask*)isTableOpenSuccess:(void (^)(BOOL open, NSString* locationId))success
                                   failure:(void (^)(MCResponseData * task, NSError *error))failure;


/* returns the users payment methods. 
 @param bid
        if set will show only payment methods that the business accepts OPTIONAL
@param orderId
        if set will show only payment methods that the business accepts OPTIONAL
 */

-(NSURLSessionDataTask*)getPaymentMethodsWithBusinessId:(NSString*)bid orderId:(NSString*)orderId success:(void (^)( NSMutableArray* methods))success
                                              failure:(void (^)(MCResponseData * response, NSError *error))failure;

/* sets the default payment method. this method will be returned as the first object in getOrderDetailsForOrderID
 
 @param type
        the type of payment method (you can get it from teh PaymentType object)
 @param cardId
        the Id of the card. needed only if the payment type is a credit card. OPTIONAL
 */
-(NSURLSessionDataTask*)setPaymentMethodasDefaultWithType:(NSString*)type cardId:(NSString*)cardId success:(void (^)())success
                                 failure:(void (^)(MCResponseData * response, NSError *error))failure;

/* sets the default payment method. this method will be returned as the first object in getOrderDetailsForOrderID
 
 @param Id
        payment method Id
 @param paypal
        true if the method is paypal
 */
-(NSURLSessionDataTask*)deletePaymentMethodWithId:(NSString*)Id isPaypal:(BOOL)paypal success:(void (^)())success
                                                  failure:(void (^)(MCResponseData * response, NSError *error))failure;

/* make a payment
 
 @param paymentDetails
 specifys the payment details
  */
-(NSURLSessionDataTask*)makePayment:(MCPaymentDetails*)paymentDetails  success:(void (^)(NSString* paymentId, BOOL askForFeedback))success
                                          failure:(void (^)(MCResponseData * response, NSError *error))failure;




-(NSURLSessionDataTask*)AddCreditCardWithNumber:(NSString*)number expiration:(NSString*)date cvv:(NSString*)cvv zip:(NSString*)zip success:(void (^)())success
                            failure:(void (^)(MCResponseData * response, NSError *error))failure;
/*
 call in order to tell if the user is logged in or not. the function may return an answer immediately or make an api call first
 
 as a convenience we return the current known state. if the state is UNKNOWN_LOGIN_STATE you should use success and fail in order to learn the state (they will be called anyway)
 @param success optional 
 @param failure optinal
 */
-(LoginState)isLoggedInSuccess:(void (^)(BOOL loggedIn,BOOL hasPaymentMethod))success
                 failure:(void (^)(MCResponseData * response, NSError *error))failure;
//-(void)getClientCodeAndCheckLoyaltyForBusinessID:(NSString*)BID PIN:(NSString*)pin
//                          success:(void (^)( NSString* code,NSString* text,bool payFull))success
//                          failure:(void (^)(ResponseData * response, NSError *error))failure;
-(NSURLSessionDataTask*)authenticatePayPalWithAuthCode:(NSString*)auth locationId:(NSString*)locationId metaDataId:(NSString*) metadataId success:(void (^)())success
                                               failure:(void (^)(MCResponseData * response, NSError *error))failure;
@property (nonatomic,readwrite) bool justEnteredPin;

@property (nonatomic, readwrite) bool isPayPal;

@property (nonatomic, readwrite) bool hasPaymentMethod;

@property (nonatomic, readwrite) bool openTable;

-(NSString*)userAgentString;
- (NSString*)getURLPrefixString;
@end
