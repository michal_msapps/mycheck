//
//  NSBundle+MCBundle.m
//  MyCheck
//
//  Created by Michal Shatz on 4/9/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "NSBundle+MCBundle.h"

@implementation NSBundle (MCBundle)

// Load the mycheck bundle.
+ (NSBundle *)mycheckBundle {
    static NSBundle* frameworkBundle = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        NSString* mainBundlePath = [[NSBundle mainBundle] resourcePath];
        NSString* frameworkBundlePath = [mainBundlePath stringByAppendingPathComponent:@"MyCheck.bundle"];
        frameworkBundle = [NSBundle bundleWithPath:frameworkBundlePath];
    });
    return frameworkBundle;
}

-(UIImage *)imageName:(NSString *)imageName{
    NSString *filePath = [[NSBundle mycheckBundle] pathForResource:imageName ofType:@"png"];
   return [UIImage imageWithContentsOfFile:filePath];
}

@end
