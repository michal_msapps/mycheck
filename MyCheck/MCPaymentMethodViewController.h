//
//  MCPaymentMethodViewController.h
//  MyCheck
//
//  Created by Michal Shatz on 3/12/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"
#import "MCPaymentMethod.h"
#import <PayPal-iOS-SDK/PayPalMobile.h>

@interface MCPaymentMethodViewController : MCBaseViewController <PayPalProfileSharingDelegate>

@end
