//
//  MCBaseViewControllerWithKeyBoard.m
//  MyCheck
//
//  Created by Michal Shatz on 4/17/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCBaseViewControllerWithKeyBoard.h"

@implementation MCBaseViewControllerWithKeyBoard

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    [super textFieldDidBeginEditing:textField];
    UIColor *editingTextFieldColor =[UIColor colorWithRed:80/255.0 green:186/255.0 blue:228/255.0 alpha:1];
    [super textFieldDidBeginEditing:textField];
    textField.layer.cornerRadius=5.0f;
    textField.layer.masksToBounds = YES;
    textField.layer.borderColor = editingTextFieldColor.CGColor;
    textField.layer.borderWidth= 1.0f;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    UIColor *nonEditingTextFieldColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.08];
    textField.layer.cornerRadius=5.0f;
    textField.layer.masksToBounds = YES;
    textField.layer.borderColor = nonEditingTextFieldColor.CGColor;
    textField.layer.borderWidth= 1.0f;
    return YES;
}


@end
