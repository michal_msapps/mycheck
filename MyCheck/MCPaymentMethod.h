//
//  MCPaymentMethod.h
//  MyCheck
//
//  Created by Michal Shatz on 3/22/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE

@interface MCPaymentMethod : UIButton

@end
