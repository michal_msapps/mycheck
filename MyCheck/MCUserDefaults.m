//
//  MCUserDefaults.m
//  MyCheck
//
//  Created by Michal Shatz on 3/24/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "MCUserDefaults.h"
#import "MSFileStoreManager.h"

#define MCUserDefaultName @" MCUserDefault"

@interface MCUserDefaults ()

@property (nonatomic, strong) NSString *path;
@property (nonatomic, strong) NSDictionary *userDefaults;

@end

@implementation MCUserDefaults

-(instancetype)init{
    self = [super init];
    if (self) {
        NSError *error = [MSFileStoreManager createFileIfDoesNotExistWithName:MCUserDefaultName andExtension:@"plist"];
        if (!error) {
            _path = [MSFileStoreManager pathForFileName:MCUserDefaultName withType:@"plist"];
        }
    }
    return self;
}

-(NSDictionary *)userDefaults{
    if (!_userDefaults) {
        _userDefaults = [[NSDictionary alloc] initWithContentsOfFile:_path];
    }
    return _userDefaults;
}

-(id)valueForKey:(NSString *)key{
    return self.userDefaults[key];
}

-(void)setValue:(id)value forKey:(NSString *)key{
    @synchronized(self) {
        NSMutableDictionary *mutableUserDefaults = [[NSMutableDictionary alloc] initWithDictionary:self.userDefaults];
        if (value) {
            [mutableUserDefaults setObject:value forKey:key];
        }
        _userDefaults = mutableUserDefaults;
        [_userDefaults writeToFile:_path atomically:NO];
    }
}

-(void)removeValueForKey:(NSString *)key{
    @synchronized(self) {
        NSMutableDictionary *mutableUserDefaults = [[NSMutableDictionary alloc] initWithDictionary:self.userDefaults];
        if ([mutableUserDefaults objectForKey:key]) {
            [mutableUserDefaults removeObjectForKey:key];
        }
        _userDefaults = mutableUserDefaults;
        [_userDefaults writeToFile:_path atomically:NO];
    }
}

@end
