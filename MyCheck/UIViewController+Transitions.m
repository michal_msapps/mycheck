//
//  UIViewController+Transitions.m
//  MyCheck
//
//  Created by Michal Shatz on 3/29/15.
//  Copyright (c) 2015 Michal MSApps. All rights reserved.
//

#import "UIViewController+Transitions.h"

@implementation UIViewController (Transitions)

-(void)presentViewController:(UIViewController *)viewControler withPushAnimationAndCompletion:(void (^)(void))completion{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self presentViewController:viewControler animated:NO completion:completion];
}
     
-(void)dismissViewControllerWithPushAnimationAndCompletion:(void (^)(void))completion{
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:completion];
}

@end
